#ifndef _UTILS_CPP_XDELEGATE_STAGE_TESTER_H_
#define _UTILS_CPP_XDELEGATE_STAGE_TESTER_H_

#define UtilsCreateXDelegateStageTester( _name_, _format_) Utils::XDelegateStageTester * _name_ = new Utils::XDelegateStageTester(); { Utils::XDelegateStageTester::XTest test [] = test_list ; _name_->SetUp(test, sizeof(test) / sizeof(Utils::XDelegateStageTester::XTest), _format_);}
namespace Utils
{
	class XDelegateStageTester
	{
	public:

		struct XTest
		{
			const char *name;

			void(*callback)();
		};

		XDelegateStageTester();

		void SetUp(XTest *tests, size_t count, const char *format);

		void Run(bool bWait = false);

	protected:

		XTest *			tests;
		size_t			count;
		const char *	format;
	};
}

#endif