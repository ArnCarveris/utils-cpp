#include "XComponentBase.h"

namespace Utils
{
	namespace Component
	{
		void XBase::InitializeBases(TBases *components)
		{
			size_t i = 0, l = components->size();

			do{

				for (; i < l; i++)
				{
					components->at(i)->Initialize(this);
				}

				l = components->size();

			} while (i < l);
		}

		void XBase::Update(XBase *entity)
		{
			TBases components = GetAll();

			for (size_t i = 0, c = components.size(); i < c; i++)
			{
				components.at(i)->Update(this);
			}
		}

		void XBase::Shutdown(XBase *entity)
		{
			TBases components = GetAll();

			for (size_t i = 0, c = components.size(); i < c; i++)
			{
				components.at(i)->Shutdown(this);
			}
		}


		void XBase::Register(const char *name, EventDelegate d)
		{
			eventHandler.Bind(name, d);
		}

		void XBase::UnRegister(const char *name)
		{
			if (name == NULL)
			{
				eventHandler.UnBind();
			}
			else
			{
				eventHandler.UnBind(name);
			}
		}

		void XBase::Send(const char *name, IEventArgs *args)
		{
			eventHandler.Emit(name, EventDelegate::TInvoker(this, args));
		}

		void XBase::Initialize(XBase *entity)
		{
			eDependence = EDependence::None;

			for (int i = 0; i < (int)components.GetCount(); i++)
			{
				InitializeBases(components.GetAtIndex(i));
			}

			Send("OnInitialize");
		}
	}
}