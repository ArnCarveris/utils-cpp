#define _ALLOW_KEYWORD_MACROS
//#define UtilsCoroutineUseSwitchBased

#include "../Utils.h"
#include "../XDelegate.h"
#include "../XDelegateStageTester.h"
#include "../XComponentSet.h"
#include "../XBit.h"
#include "../XSet.h"
#include "../XCoroutine.h"
#include "../XComponentBase.h"
#include "../XComponentNull.h"
#include "../XEvent.h"

void test_component_container();
void test_component_base();
void test_collections();
void test_bits();
void test_coroutine();
void test_coroutine_delegate();
void test_event();
void test_va();
void test_stereo_pos();
void test_va_macro();
void test_property_buffer();
void test_calculator();

#define LOG(format, __VA_DEF__) printf(format __VA_ARGS__) 

int main(int argc, char* argv[])
{
#define test_list \
{ \
	{ "property buffer", &test_calculator}, \
	{ "property buffer", &test_property_buffer}, \
	{ "stereo pos", &test_stereo_pos}, \
	{ "va test", &test_va}, \
	{ "va macro test", &test_va_macro}, \
	{ "coroutine", &test_coroutine }, \
	{ "coroutine delegate", &test_coroutine_delegate }, \
	{ "component set", &test_component_container }, \
	{ "component base", &test_component_base }, \
	{ "collections implementation", &test_collections }, \
	{ "delegate event testing", &test_event }, \
	{ "bits testing", &test_bits }, \
}
	UtilsCreateXDelegateStageTester(tester, "\n[Utils test: %s]\n")

	tester->Run(true);

	delete tester;
	
#undef test_list
	return 0;
}

void test_va_macro()
{
#define __VA_FUNC_NAME__ LOG
#define __VA_FUNC_PARAM__ "working %s -> %s -> %s"
#define __VA_FUNC_ARGS__ , "1", "2", "3"
#include "../vaargs.h"

}

class A
{
public:
	A() { }
};


UtilsDefineComponentTMessageCallback(AMessage, A*, int,
{
	printf("\nAMessage: %p, %d", component, message);
})

UtilsDefineComponentTMessageCallback(BaseMessage, void*, int,
{
	printf("\nBaseMessage: %p, %d", component, message);
})

UtilsDefineComponentTSenderMessageCallback(ASenderMessage, A*, const char *, int,
{
	printf("\nASenderMessage: %p, %s, %d", component, sender, message);
})

UtilsDefineComponentTSenderMessageCallback(BaseSenderMessage, void*, const char *, int,
{
	printf("\nBaseSenderMessage: %p, %s, %d", component, sender, message);
})

void test_component_container()
{
	Utils::Component::XContainer * c = new Utils::Component::XContainer();

	unsigned int iID = 0;

	c->Create<A>();

	c->Has<A>(iID);
	c->Has<A>();
	c->Has();

	c->GetAll<A>();
	c->GetAll();

	c->GetSize<A>();
	c->GetSize();

	c->GetCount();

	A *a = c->Get<A>();

	c->Set(a);
	c->Set<A>(a);

	c->Add(a);
	c->Add<A>(a);

	/* Component Container/Set Messaging

	== First-Call: send anonymous message
	== Second-Call: send message with signed sender, sender can be any type
	
	*/

	//BEGIN

	int iMessage = 1;
	const char *szSender = "The Sender";

	AMessageDelegate aMessage = AMessageDelegate::FromFunction<AMessage>();
	ASenderMessageDelegate aSenderMessage = ASenderMessageDelegate::FromFunction<ASenderMessage>();

	BaseMessageDelegate baseMessage = BaseMessageDelegate::FromFunction<BaseMessage>();
	BaseSenderMessageDelegate baseSenderMessage = BaseSenderMessageDelegate::FromFunction<BaseSenderMessage>();

	
	printf("\n\n\t[Direct sending to specific component ID]\n");

	c->Send<A, int>(iMessage, &aMessage, iID);
	c->Send<A, const char*, int>(szSender, iMessage, &aSenderMessage, iID);

	printf("\n\n\t[Indirect sending specified component types]\n");
	
	c->Send<A, int>(iMessage, &aMessage);
	c->Send<A, const char*, int>(szSender, iMessage, &aSenderMessage);

	printf("\n\n\t[Indirect sending to all components]\n");

	c->Send<int>(iMessage, &baseMessage);
	c->Send<const char*, int>(szSender, iMessage, &baseSenderMessage);

	//END

	c->Remove<A>();
	c->Remove();

	delete c;

}


struct IA : public Utils::Component::XNull
{
	int one;
};

struct XA : public IA
{
	int tvo;


};

struct XT
{

	static void OnInitializeFunction(Utils::Component::XBase *entity, Utils::Component::XBase::IEventArgs *args)
	{
		printf("\nEntity did initialize via function\n");
	}

	void OnInitializeMethod(Utils::Component::XBase *entity, Utils::Component::XBase::IEventArgs *args)
	{
		printf("\nEntity did initialize via method\n");
	}

	void OnInitializeConstMethod(Utils::Component::XBase *entity, Utils::Component::XBase::IEventArgs *args) const
	{
		printf("\nEntity did initialize via const method\n");
	}
};



void test_component_base()
{

	Utils::Component::XBase *b = new Utils::Component::XBase();
	IA *a = NULL;
	XA *aa = NULL, *aaa = NULL;

	b->Create<IA, XA>();


	b->Require<IA>();

	b->Require<IA, XA>();

	b->Require<IA, XA>(Utils::Component::XBase::ERequirePolicy::CreateIfNeeded, 3);

	b->Require<XA>(Utils::Component::XBase::ERequirePolicy::CreateIfNeeded, 4);
	
	XT xt;
	b->RegisterFunction<&XT::OnInitializeFunction>("OnInitialize");
	b->RegisterMethod<XT, &XT::OnInitializeMethod>("OnInitialize", &xt);
	b->RegisterConstMethod<XT, &XT::OnInitializeConstMethod>("OnInitialize", &xt);

	b->Initialize(NULL);

	b->Add<IA>(a);

	

	delete b;
}

namespace Collection
{
	struct XItem
	{
		int n;

		XItem(int n_)
		{
			n = n_;
		}

		bool operator==(const XItem &other) const
		{
			return n == other.n;
		}
		
		bool operator!=(const XItem &other) const
		{
			return n != other.n;
		}
	};

	void testItem()
	{
		Utils::XSet<XItem> set;

		set.Add(XItem( 1 ));
		set.Add(XItem(  2 ));

		set.Add(XItem(  5 ));
		set.Add(XItem(  13 ));

		const bool bHas = set.Has(XItem(  2 ));

		set[3].n = 6;
	}

	void testNewItem()
	{

		Utils::XSet<XItem*> set;

		set.Add(new XItem(1));
		set.Add(new XItem(2));

		set.Add(new XItem(5));
		set.Add(new XItem(13));

		const bool bHas = set.Has(set[1]);

		set[2]->n = 6;
	}
	void testU()
	{
		Utils::XSet<unsigned int> set;

		set.Add(1);
		set.Add(2);

		set.Add(5);
		set.Add(13);

		const bool bHas = set.Has(2);

		set[2] = 6;
	}
}

void test_collections()
{
	Collection::testItem();
	Collection::testNewItem();
	Collection::testU();

}

void test_bits()
{
	unsigned int n;

	printf("Enter absoliute number: ");

	scanf("%d", &n);
	
	const unsigned int c = Utils::Bit::GetCount(n);

	printf("\nbits: "); Utils::Bit::Print(n);
	printf("\nbit count: %d", c);

	n &= 0x1u << 2u;

	printf("\nmask: %d", n);
	printf("\nbits: "); Utils::Bit::Print(n);
}

#ifdef UtilsCoroutineUseSwitchBased

UtilsCoroutine(bool, descent, int &v _(int &k) _(int &m),
	int i _(j);
	, {
		i = 10;
	}) {

	while (--i > 0)
	{
		if (i % 2 == 0)
		{
			UtilsCoroutineYield(
				m = -i;
				k = 0;
				v = i;
			, true);
		}
		else
		{
			UtilsCoroutineYield(
				m = -i;
				k = 1;
				v = i;
			, true);
		}
	}
} UtilsCoroutineEnd(
		printf("end");
, false);

void test_coroutine()
{
	descent gen;

	for (int n, k, m; gen(n, k, m);)
	{
		printf("next number is [%d, %d, %d]\n", n, k, m);
	}
}

#else

//type, name, arguments, vars, pre_call_with_args
UtilsCoroutine(unsigned, fibonacci, int &n, 
	unsigned i;
	, {
	printf(" %d:", n);
}) {
	unsigned a = 0, b = 1;

	while (++i)
	{
		yield(b);

		b = b + a;
		a = b - a;
	}
}} FibonacciCoroutine;

UtilsCoroutine(bool, descent, int &v, 
	int *ptrV;
	, {
		ptrV = &v;
}) {
	
	*ptrV = 10;
	while (--*ptrV > 0)
	{
		yield(true);
	}
	yield(false);

}} DescentCoroutine;



void test_coroutine()
{
	
	for (int i = 0; i<20; i++)
	{
		printf("%i ", FibonacciCoroutine(i));
	}
	printf("\n\n");
	
	for (int n; DescentCoroutine(n);)
	{
		printf("next number is %d\n", n);
	}

}


typedef_utils_event_x(1<void _(const char*)>, TestEvent,,
	TBasic g_basic;
	TMapped g_mapped;
);

namespace Event
{
	typedef Utils::Delegate::X1<void, const char*> TheDelegate;
	typedef Utils::Event::XSource<TheDelegate> TestEventSource;
	typedef Utils::Event::XHandler<TheDelegate> TestEventHandler;

	TestEventSource			g_source;
	TestEventHandler		g_handler;

	TestEvent::TBasic g_basic;
	TestEvent::TMapped g_mapped;

	void test_function1(const char* message) { printf("Test function 1 called with message: %s\n", message); }
	void test_function2(const char* message) { printf("Test function 2 called with message: %s\n", message); }
	void test_function3(const char* message) { printf("Test function 3 called with message: %s\n", message); }
}


void test_event()
{
	
	Event::g_source.Bind(Event::TheDelegate::FromFunction<&Event::test_function1>());
	Event::g_source.Bind(Event::TheDelegate::FromFunction<&Event::test_function2>());
	Event::g_source.Bind(Event::TheDelegate::FromFunction<&Event::test_function3>());
	
	Event::g_source.Emit(Event::TheDelegate::TInvoker("Event"));
	
	Event::g_handler.Bind("S123", Event::TheDelegate::FromFunction<&Event::test_function1>());
	Event::g_handler.Bind("S123", Event::TheDelegate::FromFunction<&Event::test_function2>());
	Event::g_handler.Bind("S123", Event::TheDelegate::FromFunction<&Event::test_function3>());

	Event::g_handler.Bind("S23", Event::TheDelegate::FromFunction<&Event::test_function2>());
	Event::g_handler.Bind("S23", Event::TheDelegate::FromFunction<&Event::test_function3>());

	Event::g_handler.Bind("S12", Event::TheDelegate::FromFunction<&Event::test_function1>());
	Event::g_handler.Bind("S12", Event::TheDelegate::FromFunction<&Event::test_function2>());

	Event::g_handler.Emit("S123", Event::TheDelegate::TInvoker("S123"));
	Event::g_handler.Emit("S23", Event::TheDelegate::TInvoker("S23"));
	Event::g_handler.Emit("S12", Event::TheDelegate::TInvoker("S12"));


	TestEvent::g_basic.hInvoker = TestEvent::TInvoker("Test Event");
	TestEvent::g_basic.Bind(TestEvent::TDelegate::FromFunction<&Event::test_function1>());
	TestEvent::g_basic.Bind(TestEvent::TDelegate::FromFunction<&Event::test_function2>());
	TestEvent::g_basic.Bind(TestEvent::TDelegate::FromFunction<&Event::test_function3>());
	TestEvent::g_basic.Emit();
	TestEvent::g_basic.UnBind();
	
	TestEvent::g_mapped.Bind("S123", TestEvent::TDelegate::FromFunction<&Event::test_function1>());
	TestEvent::g_mapped.Bind("S123", TestEvent::TDelegate::FromFunction<&Event::test_function2>());
	TestEvent::g_mapped.Bind("S123", TestEvent::TDelegate::FromFunction<&Event::test_function3>());

	TestEvent::g_mapped.Bind("S23", TestEvent::TDelegate::FromFunction<&Event::test_function2>());
	TestEvent::g_mapped.Bind("S23", TestEvent::TDelegate::FromFunction<&Event::test_function3>());

	TestEvent::g_mapped.Bind("S12", TestEvent::TDelegate::FromFunction<&Event::test_function1>());
	TestEvent::g_mapped.Bind("S12", TestEvent::TDelegate::FromFunction<&Event::test_function2>());

	TestEvent::g_mapped.Emit("S123", TestEvent::TDelegate::TInvoker("event with 1,2,3"));
	TestEvent::g_mapped.Emit("S23", TestEvent::TDelegate::TInvoker("event with 2, 3"));
	TestEvent::g_mapped.Emit("S12", TestEvent::TDelegate::TInvoker("event with 1, 2"));
	TestEvent::g_mapped.UnBind();
}

namespace CoroutineDelegate
{
	typedef Utils::Delegate::Coroutine::XBase<int> CoroutineBase;
	typedef Utils::Delegate::Coroutine::I1<int, int> CoroutineInterface;
	typedef Utils::Delegate::Coroutine::X1<int, int> CoroutineTemplate; // Aka Delegate

	
	struct XFibonacci : CoroutineInterface
	{
		XFibonacci() : CoroutineInterface(0) {}

		void Prepare(int i)
		{
			printf(" %d:", i);
		}

		void Invoke(int i)
		{

			unsigned a = 0, b = 1;

			while (true)
			{
				Yield(b);

				b = b + a;
				a = b - a;
			}
		}
	} FibonacciClass;

	void FibonacciFunction(CoroutineBase *coroutine, int a1)
	{
		unsigned a = 0, b = 1;

		while (true)
		{
			coroutine->Yield(b);

			b = b + a;
			a = b - a;
		}
	}

	void PrepareFunction(int i)
	{
		printf(" %d:", i);
	}
}


void test_coroutine_delegate()
{

	for (int i = 0; i<20; i++)
	{
		printf("%i ", CoroutineDelegate::FibonacciClass(i));
	}

	CoroutineDelegate::CoroutineTemplate x = CoroutineDelegate::CoroutineTemplate(0);
	CoroutineDelegate::CoroutineTemplate::TDelegate d = CoroutineDelegate::CoroutineTemplate::TDelegate::FromFunction<&CoroutineDelegate::FibonacciFunction>();
	CoroutineDelegate::CoroutineTemplate::TCallback c = CoroutineDelegate::CoroutineTemplate::TCallback::FromFunction<&CoroutineDelegate::PrepareFunction>();
	
	printf("\n\n");


	for (int i = 0; i<20; i++)
	{
		printf("%i ", x(d, c, i));
	}


}
#include "../stdargex.h"
#include <vector>


namespace va
{
	void print_item(const char *item)
	{
		printf(" %s", item);
	}

	void print_items(const char **items, size_t count)
	{
		for(size_t i = 0; i < count; i++)
		{
			print_item(items[i]);
		}
	}

	void test_simple(va_param(const char*))
	{
		va_process_each(const char*, string,
		{
			print_item(string);
		});
	}

	va_function_ref(const char*, void, test_array);

	va_method(const char*, void, test_array)
	{
		va_list_array_new(const char*, items, count);
		
		print_items(items, count);

		delete[] items;
	}


	va_function_ref(const char*, void, test_array_ex);

	va_method(const char*, void, test_array_ex)
	{
		va_list_array_new_ex(const char*,const char*, items, count, (*va_value_dst)=(*va_value_src););
		
		print_items(items, count);

		delete[] items;
	}

	void test_array_in(va_param(const char*))
	{
		va_array_new(const char*, items, count);
		
		print_items(items, count);

		delete[] items;
	}
	
	void test_array_in_ex(va_param(const char*))
	{
		va_array_new_ex(const char*,const char*, items, count, (*va_value_dst)=(*va_value_src););
		
		print_items(items, count);

		delete[] items;
	}

}

#define va_test_args(_name_) printf("\n<%s>:\t ", #_name_);va::test_##_name_ ("a", "bc", "cdf", "ghij", NULL) 

void test_va()
{
	va_test_args(simple);
	va_test_args(array_in);
	va_test_args(array);
	va_test_args(array_in_ex);
	va_test_args(array_ex);
}

#endif

#include <math.h>

class Vector2D
{
    public:
    
        float   x;
        float   y;
        
        Vector2D()
		{
			x = 0.0f;
			y = 0.0f;
		}
        
        Vector2D(float r, float s)
        {
            x = r;
            y = s;
        }

        Vector2D& Set(float r, float s)
        {
            x = r;
            y = s;
            return (*this);
        }
        
        float& operator [](long k)
        {
            return ((&x)[k]);
        }
        
        const float& operator [](long k) const
        {
            return ((&x)[k]);
        }
        
        Vector2D& operator +=(const Vector2D& v)
        {
            x += v.x;
            y += v.y;
            return (*this);
        }
        
        Vector2D& operator -=(const Vector2D& v)
        {
            x -= v.x;
            y -= v.y;
            return (*this);
        }
        
        Vector2D& operator *=(float t)
        {
            x *= t;
            y *= t;
            return (*this);
        }
        
        Vector2D& operator /=(float t)
        {
            float f = 1.0F / t;
            x *= f;
            y *= f;
            return (*this);
        }
        
        Vector2D& operator &=(const Vector2D& v)
        {
            x *= v.x;
            y *= v.y;
            return (*this);
        }
        
        Vector2D operator -(void) const
        {
            return (Vector2D(-x, -y));
        }
        
        Vector2D operator +(const Vector2D& v) const
        {
            return (Vector2D(x + v.x, y + v.y));
        }
        
        Vector2D operator -(const Vector2D& v) const
        {
            return (Vector2D(x - v.x, y - v.y));
        }
        
        Vector2D operator *(float t) const
        {
            return (Vector2D(x * t, y * t));
        }
        
        Vector2D operator /(float t) const
        {
            float f = 1.0F / t;
            return (Vector2D(x * f, y * f));
        }
        
        float operator *(const Vector2D& v) const
        {
            return (x * v.x + y * v.y);
        }
        
        Vector2D operator &(const Vector2D& v) const
        {
            return (Vector2D(x * v.x, y * v.y));
        }
        
        bool operator ==(const Vector2D& v) const
        {
            return ((x == v.x) && (y == v.y));
        }
        
        bool operator !=(const Vector2D& v) const
        {
            return ((x != v.x) || (y != v.y));
        }
        
		float SqLength()
		{
			return x * x + y * y;
		}

		float Length()
		{
			return sqrtf(SqLength());
		}

        Vector2D& Normalize(void)
        {
            return (*this /= Length());
        }
        Vector2D& Rotate(float angle)
		{
			float s = sinf(angle);
			float c = cosf(angle);
		    
			float nx = c * x - s * y;
			float ny = s * x + c * y;
		    
			x = nx;
			y = ny;
		    
			return (*this);
		}
};

#include "../MathXIntersector.h"

#include <fstream>
void test_stereo_pos()
{
	const char *szFileName = "C:\\Data\\sps.csv";
	int iCode;
	
	float fAngle[2];
	int count = 0;
	float fAccuracyBias = 0.9900000f;
	float fMaxDegradation = 0.0f;

	Vector2D vInitPosition = Vector2D();
	Vector2D vGoalPosition = Vector2D(500.0f, 4.0f);
	Vector2D vDir = Vector2D(1, 1);

	FILE *hFile = fopen (szFileName, "w");

	if (hFile == NULL )
	{
		perror(szFileName);

		return;
		
	}

	fprintf(hFile, "x:%f:%f,y:%f:%f,x[0],y[0],x[1],y[1],d[0].x,d[0].y,d[1].x,d[1].y,Mode,Code,Angle,Dot,Len\n", vInitPosition.x,vGoalPosition.x,vInitPosition.y,vGoalPosition.y);

#define GoalDirection(info)  (vGoalPosition - info.vPosition).Normalize()
#define GoalInvertedDirection(info)  (info.vPosition - vGoalPosition).Normalize()


	Utils::Math::XIntersector<Vector2D>::Input hInput[2] = {
		{ vInitPosition, vGoalPosition },
		{ vInitPosition, vGoalPosition }
	};

	Utils::Math::XIntersector<Vector2D> intersector = Utils::Math::XIntersector<Vector2D>(hInput);

	for (int i = 0; i < 2; i++)
	{
		intersector.hInput[i].vDirection = GoalDirection(intersector.hInput[i]);
	}

	intersector.Reset();

	for (int iAngle = 0; iAngle < 360; iAngle += 1)
	{

#if 0
		for (int i = 0; i < 2; i++)
		{
			fAngle[i] = (float)((iAngle + 45 * i) % 360);

			hInput[i].vPosition = vDir.Rotate(fAngle[i]);


		}
		
		intersector.hInput[0] = hInput[0];
		intersector.hInput[1] = hInput[1];
#else

		hInput[1].vPosition = vDir.Normalize() * iAngle;

#endif

		for(int i = 0; i < 2; i++)
		{
			hInput[i].vDirection = GoalDirection(hInput[i]);
		}

		intersector.bFilterEnabled = hInput[0].NeedsFilteringForAxises(2);

		iCode = intersector.Update(hInput[1]);

		fprintf(hFile, "%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,", 
			intersector.hOutput.vPosition.x,
			intersector.hOutput.vPosition.y,
			intersector.hInput[0].vPosition.x,
			intersector.hInput[0].vPosition.y,
			intersector.hInput[1].vPosition.x,
			intersector.hInput[1].vPosition.y,
			
			intersector.hInput[0].vDirection.x,
			intersector.hInput[0].vDirection.y,
			intersector.hInput[1].vDirection.x,
			intersector.hInput[1].vDirection.y
		);

		float fLen = (intersector.hOutput.vPosition - vGoalPosition).Length();
		
		fprintf(hFile, "%d,%d,%d,%f,%f\n", intersector.bFilterEnabled, iCode * (1 + (int)(fLen / fAccuracyBias)), iAngle, intersector.hOutput.fDot, fLen);

	}


	printf("\n\t max.len: %f, best[dot: %f, len: %f]",fMaxDegradation, intersector.fMinAbsDot, (intersector.hBestOutput.vPosition - vGoalPosition).Length());

#undef GoalDirection
#undef GoalInvertedDirection

	fclose(hFile);
}

#include "../XValueBuffer.h"
#include "../XIndexedValueBuffer.h"

XT(Value) void test_value_buffer(Utils::XValueBuffer *pBuffer, TValue hValue, const char *szValueFormat)
{
	size_t iOffset;


	TValue *pProperty = pBuffer->Set<TValue>(&iOffset);
	
	printf("\ncreated value with type '%s' size of '%d' at offset '%d'\n", typeid(TValue).name(), sizeof(TValue), iOffset);

	pBuffer->Set<TValue>(hValue, iOffset);
	
	printf("setting '");
	printf(szValueFormat,	hValue);
	printf("' -> %p: ",	pProperty);
	printf(szValueFormat, *	pProperty);

	pProperty = pBuffer->Get<TValue>(iOffset);

	printf("\ngetting %p: ",	pProperty);
	printf(szValueFormat, *	pProperty);
	printf("\n");
}

XT(Value) void test_indexed_value_buffer(Utils::XIndexedValueBuffer *pBuffer, TValue hValue, const char *szValueFormat)
{
	size_t iIndex;


	TValue *pProperty = pBuffer->Set<TValue>(&iIndex);
	
	printf("\ncreated value with type '%s' size of '%d' at index '%d'\n", typeid(TValue).name(), sizeof(TValue), iIndex);

	pBuffer->Set<TValue>(hValue, iIndex);
	
	printf("setting '");
	printf(szValueFormat,	hValue);
	printf("' -> %p: ",	pProperty);
	printf(szValueFormat, *	pProperty);

	pProperty = pBuffer->Get<TValue>(iIndex);

	printf("\ngetting %p: ",	pProperty);
	printf(szValueFormat, *	pProperty);
	printf("\n");
}

void test_value_buffer(size_t hMaxMemory)
{
	Utils::XValueBuffer hBuffer(hMaxMemory);
	
	printf("\n\t<%s>\n", typeid(hBuffer).name());

	test_value_buffer<float>(&hBuffer, 134.0f, "%f");
	test_value_buffer<int>(&hBuffer, 344, "%d");
	test_value_buffer<bool>(&hBuffer, false, "%d");
	test_value_buffer<float>(&hBuffer, 3.14f, "%f");

	printf("\n\tbuffer usage %d/%d", hBuffer.GetOffset(), hBuffer.GetSize());
}


void test_indexed_value_buffer(size_t hMaxMemory)
{
	Utils::XIndexedValueBuffer hBuffer(hMaxMemory);
	
	printf("\n\t<%s>\n", typeid(hBuffer).name());

	test_indexed_value_buffer<float>(&hBuffer, 134.0f, "%f");
	test_indexed_value_buffer<int>(&hBuffer, 344, "%d");
	test_indexed_value_buffer<bool>(&hBuffer, false, "%d");
	test_indexed_value_buffer<float>(&hBuffer, 3.14f, "%f");

	printf("\n value at index '%d'", *(int*)hBuffer.Get(1));

	printf("\n\tbuffer usage %d/%d", hBuffer.GetOffset(), hBuffer.GetSize());
}


void test_property_buffer()
{
	test_value_buffer(32);
	test_indexed_value_buffer(16);

}


#include "../MathXCalculator.h"

XT(Value) void print_code(Utils::XIndexedValueBuffer &hBuffer, size_t iIndex, const char *szFormat = "0x%X")
{
	printf("\n\t\t(%s) = ",		typeid(TValue).name());
	printf(szFormat, *hBuffer.Get < TValue >(iIndex));
}

void print_ops(const size_t kMaxMemory)
{
	Utils::XIndexedValueBuffer hBuffer(kMaxMemory);

#define buffer_op(op) hBuffer.Set<const char*>(#op)
	buffer_op(>=);
	buffer_op(<=);
	buffer_op(==);
	buffer_op(!=);
	buffer_op(&&);
	buffer_op(||);
	buffer_op(>>);
	buffer_op(<<);
	buffer_op(<);
	buffer_op(>);
	buffer_op(&);
	buffer_op(|);
	buffer_op(!);
#undef buffer_op

	for (int i = 0, l = hBuffer.GetIndexCount(); i < l; i++)
	{
		const char* op = *hBuffer.Get<const char*>(i);


		printf("\nop[%d]: '%s'", i, op);

		print_code<unsigned short>(hBuffer, i);
		print_code<unsigned int>(hBuffer, i);
		print_code<unsigned long>(hBuffer, i);
	}
}

void test_calculator()
{
	//print_ops(32);
	
#	define print_op_test( ta, tv, tb, a, _op_, b, f)\
	{\
		tv v = Utils::Math::Calculate<ta, tv, tb>(a, #_op_, b);\
		tv w = a _op_ b;\
		printf("\n testing %s %s %s = ", #a, #_op_, #b);\
		printf(f, v, w);\
	}

	print_op_test(int, int, int, 2, << , 3, "[%d, %d]");
	print_op_test(int, int, int, 2, + , 3, "[%d, %d]");
}