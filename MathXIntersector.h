#pragma once

namespace Utils
{
	namespace Math
	{
		static float MaxDot(const bool bFilterEnabled = true)
		{
			static float fDot[] = {1.0f, 0.99999900f};

			return fDot[bFilterEnabled];
		}

		XT(Vector) class XIntersector
		{
		public:

			struct Input
			{
				TVector vPosition;
				TVector vDirection;

				bool NeedsFilteringForAxises(unsigned char uAxisesCount, float fMaxDot = -1.0f)
				{
					bool bNeeds = false;

					if (fMaxDot <= -1.0f)
					{
						fMaxDot = MaxDot();
					}
					
					for (unsigned char i = 0; i < uAxisesCount; i++)
					{
						bNeeds |= vDirection[i] >= fMaxDot;
					}

					return !bNeeds;
				}
			};

			struct Data
			{
				float fProjection;
				float fDistance;
				TVector vPosition;

				Data()
				{
					fProjection = 0.0f;
					fDistance = 0.0f;
					vPosition = TVector();
				}
			};

			struct Output
			{
				float fDot;
				TVector vPosition;
				Data	hData[2];

				Output()
				{
					fDot = 0.0f;
					vPosition = TVector();
				}
			};

			static bool Perform(const Input hInput[2], Output &hOutput)
			{
#				define o(i) hOutput.hData[i]
#				define i(i) hInput[i]
				
				hOutput.fDot = i(0).vDirection * i(1).vDirection;

				if (hOutput.fDot == 1)
				{
					hOutput.vPosition = (i(0).vPosition + i(1).vPosition) * 0.5f;

					return false;
				}

				TVector	vDirection = i(1).vPosition - i(0).vPosition;
				float	fDotSq =  hOutput.fDot * hOutput.fDot;

				o(0).fProjection = vDirection * i(0).vDirection;
				o(1).fProjection = vDirection * i(1).vDirection;
				
				o(0).fDistance = (o(0).fProjection - hOutput.fDot * o(1).fProjection) / (1.0f - fDotSq);
				o(1).fDistance = (o(1).fProjection - hOutput.fDot * o(0).fProjection) / (fDotSq - 1.0f);

				o(0).vPosition = i(0).vPosition + (i(0).vDirection * o(0).fDistance);
				o(1).vPosition = i(1).vPosition + (i(1).vDirection * o(1).fDistance);

				hOutput.vPosition = (o(0).vPosition + o(1).vPosition) * 0.5f;

				return true;
#				undef i
#				undef o
			}
			

			static int UpdateBest(float &fMinAbsDot, const float fAbsDot, const bool bFilterEnabled = true)
			{
				int iCode = 0;
			
				if (fAbsDot < MaxDot(bFilterEnabled))
				{
					if (fMinAbsDot > fAbsDot || fMinAbsDot <= 0.0f)
					{
						fMinAbsDot = fAbsDot;

						iCode = 1;
					}
					else if (fMinAbsDot < fAbsDot)
					{
						iCode = -1;
					}
				}

				return iCode;
			}

			Input hInput[2];
			Output hOutput;
			Output hBestOutput;

			float fMinAbsDot;
			bool bFilterEnabled;

			XIntersector()
			{
				Reset();
			}

			
			XIntersector(Input hInput[2])
			{
				Reset();

				this->hInput[0] = hInput[0];
				this->hInput[1] = hInput[1];
			}

			void Reset()
			{
				bFilterEnabled = true;
				fMinAbsDot = 0.0f;
				hOutput = Output();
			}

			int UpdateBestOutput()
			{
				return UpdateBestOutput(this->bFilterEnabled);
			}

			int UpdateBestOutput(bool bFilterEnabled)
			{
				const int iCode = UpdateBest(fMinAbsDot, fabsf(hOutput.fDot), bFilterEnabled);

				if (iCode == 1)
				{
					hBestOutput = hOutput;
				}

				return iCode;
			}

			int Update(Input &hInput)
			{
				int iCode = 0;
				
				Input hOldInput = this->hInput[1];
				
				this->hInput[1] = hInput;
				
				if (Perform(this->hInput, hOutput))
				{
					iCode = UpdateBestOutput();

					if (iCode == 1 && fMinAbsDot < MaxDot())
					{
						this->hInput[0] = hOldInput;
					}
				}

 				return iCode;
			}

			bool Perform()
			{
				return Perform(hInput, hOutput);
			}
		};
	}
	
}