#pragma once

#include "IKernel.h"
#include "Interface.h"


namespace Utils
{
	namespace Plugin
	{
		struct IXKernel : public IKernel
		{
			template <typename T> T* GetInterface(size_t nIndex = 0)
			{
				return nIndex == (size_t)(-1) ? NULL : (T*)GetInterfaceWithID(T::GetID(), nIndex);
			}
			
			template <typename T> IKernel* RegisterAs(T *pT)
			{
				Register((void*)pT, T::GetID(), 0);

				return (IXKernel*)this;
			}

			template <typename T> IKernel* RegisterAs(void *pV, T *pT)
			{
				Register(pV, T::GetID(), (size_t*)(pT) - (size_t*)(pV));

				return (IXKernel*)this;
			}

			template <typename T> IKernel* Unregister(void *pT = 0)
			{
				Register(pT, T::GetID(), false);
				
				return (IXKernel*)this;
			}		
		};
		
	}
}