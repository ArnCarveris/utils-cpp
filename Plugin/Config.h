#pragma once

#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_APP)
  #define MODULE_WINRT 1
#elif defined(WIN32) || defined(_WIN32)
#define MODULE_WIN32 1
#else
#define MODULE_LINUX 1
#endif

#if defined(_MSC_VER)

  #if defined(MODULE_STATICLIB)
    #define MODULE_API
  #else
    #if defined(MODULE_SOURCE)
      #define MODULE_API __declspec(dllexport)
    #else
		#define MODULE_API __declspec(dllimport)
    #endif
  #endif

#elif defined(__GNUC__)

  #if defined(MODULE_STATICLIB)
    #define MODULE_API
  #else
    #if defined(MODULE_SOURCE)
      #define MODULE_API __attribute__ ((visibility ("default")))
    #else
      // If you use -fvisibility=hidden in GCC, exception handling and RTTI
      // would break if visibility wasn't set during export _and_ import
      // because GCC would immediately forget all type infos encountered.
      // See http://gcc.gnu.org/wiki/Visibility
      #define MODULE_API __attribute__ ((visibility ("default")))
    #endif
  #endif

#else

  #error Unknown compiler, please implement shared library macros

#endif


#define MODULE_METHOD MODULE_API virtual
#define MODULE_FUNCTION extern "C" MODULE_API