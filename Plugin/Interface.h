#pragma once
#include <functional>
#include <algorithm>
#include <vector>

#include <iostream>
#include <assert.h>

#include "../XPointer.h"
#include "IKernel.h"

#define InterfaceRegister(_name_) static __ ## _name_
#define TypedInterfaceRegister(a, _name_) static a __ ## _name_

namespace Utils
{
	namespace Plugin
	{
		struct Interface
		{
			static size_t Address(void *pObject, size_t i = 0)
			{
				return *((size_t*)pObject + i);
			}

			static size_t Address(void *pObject, size_t x, size_t y, bool bDeepFirst = true)
			{
				if (bDeepFirst)
				{
					while(--y)
					{
						pObject = (void*)Address(pObject);
					}
					
					pObject = (void*)Address(pObject, x);
				}
				else
				{
					pObject = (void*)Address(pObject, x);

					while(--y)
					{
						pObject = (void*)Address(pObject);
					}
				}

				return (size_t)pObject;
			}

			static void UpdateInterfaceOffsets(size_t *pBegin, size_t *pEnd, Pointer::IInterfaceOffset *pIO, size_t x, size_t y, bool bClear = false)
			{
				size_t nAddress;

				if (bClear)
				{
					pIO->Clear();
				}

				for(size_t *pInterface = pBegin; pInterface < pEnd; pInterface++)
				{
					nAddress = Interface::Address(pInterface, x, y);

					if (Pointer::IsMagical(nAddress))
					{
						continue;
					}
					size_t nID = Interface::GetID(nAddress, false);
					size_t nOffset = pInterface - pBegin;
					
					pIO->Add(nID, nOffset);
				}
			}
			
			static size_t GetID(size_t nAddress, bool bAddIfNeeded = true)
			{
				if (nAddress == 0)
				{
					return 0;
				}

				static std::vector<size_t> vAddresses;
				size_t nAddresses = vAddresses.size();

				std::vector<size_t>::iterator it = std::find(vAddresses.begin(), vAddresses.end(), nAddress);

				if (!nAddresses || it == vAddresses.end())
				{
					if (bAddIfNeeded)
					{
						vAddresses.push_back(nAddress);

						return 1 + nAddresses;
					}
					else
					{
						return 0;
					}
				}

				return 1 + (it - vAddresses.begin());
			}

			template <typename T> struct As
			{
				typedef Utils::Pointer::Base<T>* TPointer;
				typedef std::vector<TPointer>	TVector;
				
				As()
				{
					if (GetID() == 0)
					{
						GetID(Interface::GetID(Interface::Address(this, 0, 2)));
					}
				}

				virtual size_t GetVirtualID() { return GetID();}

				static const size_t GetID(size_t nID = -1)
				{
					static size_t n = 0;

					if (nID != (size_t)(-1) && !n)
					{
						n = nID;
					}

					return n;
				}

				static void *GetVector(TVector &rVector)
				{
					return (void*)(&rVector);
				}

				
				static T * Get(typename const TVector::iterator &it)
				{
					return (*it)->Get<T>();
				}

				static T * Get(typename TVector::iterator &it)
				{
					return (*it)->Get<T>();
				}

				static T * Get(void *pObject)
				{
					return Pointer::Get<T>(pObject);
				}
			};
		};
	}
}