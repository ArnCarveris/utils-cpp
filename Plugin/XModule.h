#pragma once
#include "SharedLibrary.h"
#include "IKernel.h"
#include "IModule.h"
namespace Utils
{
	namespace Plugin {

		class CXModule : public IModule
		{
		public:  
			CXModule() :
				sharedLibraryHandle(0),
				referenceCount(0),
				onModuleLoadAddress(0),
				onModuleUnLoadAddress(0),
				setModuleNameAddress(0)
			{ }
			CXModule(const char *szFilename) :
				sharedLibraryHandle(0),
				referenceCount(0),
				onModuleLoadAddress(0),
				onModuleUnLoadAddress(0),
				setModuleNameAddress(0)
			{
				this->sharedLibraryHandle = SharedLibrary::Load(szFilename);

				try {

					Retain();

					this->onModuleLoadAddress = GetFunctionPointer < OnModuleLoadFunction >("OnModuleLoad");
					this->onModuleUnLoadAddress = GetFunctionPointer < OnModuleUnLoadFunction >("OnModuleUnLoad");
				
					if ((this->setModuleNameAddress = GetFunctionPointer <SetModuleNameFunction>("SetModuleName")))
					{
						this->setModuleNameAddress(szFilename);
					}
				}
				catch (std::exception &) {
					Release();
					throw;
				}
			}
		
			CXModule(const CXModule *pOther) :
				sharedLibraryHandle(pOther->sharedLibraryHandle),
				referenceCount(pOther->referenceCount),
				onModuleLoadAddress(pOther->onModuleLoadAddress),
				onModuleUnLoadAddress(pOther->onModuleUnLoadAddress),
				setModuleNameAddress(pOther->setModuleNameAddress)
			{
				Retain();
			}
			
			CXModule(const CXModule &other) :
				sharedLibraryHandle(other.sharedLibraryHandle),
				referenceCount(other.referenceCount),
				onModuleLoadAddress(other.onModuleLoadAddress),
				onModuleUnLoadAddress(other.onModuleUnLoadAddress),
				setModuleNameAddress(other.setModuleNameAddress)
			{
				Retain();
			}

			virtual ~CXModule() {
				Release();
			}


			bool RegisterModule(IXKernel *kernel)
			{
				if (!onModuleLoadAddress || !onModuleUnLoadAddress || !setModuleNameAddress)
				{
					return false;
				}

				onModuleLoadAddress(kernel);

				return true;
			}

			CXModule &operator =(const CXModule &other);
	
		protected:
	
			template <typename TFunction> TFunction *GetFunctionPointer(const char *szName = NULL)
			{
				return SharedLibrary::GetFunctionPointer< TFunction>(this->sharedLibraryHandle, szName);
			}

			bool Release()
			{
				if (this->referenceCount)
				{
					int remainingReferences = --*(this->referenceCount);

					if (remainingReferences > 0)
					{
						return false;
					}

					delete this->referenceCount;

					this->referenceCount = 0;
				}
			
				if (this->sharedLibraryHandle)
				{
					if (this->onModuleUnLoadAddress)
					{
						this->onModuleUnLoadAddress();
						this->onModuleUnLoadAddress = NULL;
					}

					SharedLibrary::Unload(this->sharedLibraryHandle);

					this->sharedLibraryHandle = NULL;
				}
			
				return true;
			}

			void Retain()
			{
				if (this->referenceCount) {
					++(*this->referenceCount);
				}
				else
				{
					this->referenceCount = new size_t(1);
				}
			}

		private:
		
			typedef void OnModuleLoadFunction(IXKernel *);
			typedef void OnModuleUnLoadFunction();
			typedef void SetModuleNameFunction(const char*);

			SharedLibrary::HandleType sharedLibraryHandle;
			size_t *referenceCount;

			OnModuleLoadFunction *onModuleLoadAddress;
			OnModuleUnLoadFunction *onModuleUnLoadAddress;
			SetModuleNameFunction *setModuleNameAddress;

		};
	}
}