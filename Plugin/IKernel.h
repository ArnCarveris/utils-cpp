#pragma once

#include <utility> 

namespace Utils
{
	namespace Pointer
	{
		struct IInterfaceOffset;
	}
	namespace Plugin
	{
		struct IKernel
		{			
			typedef void*(*InterfaceAccessorThunk)(void *pTarget, size_t nIndex);
			typedef void(*InterfaceRegisterThunk)(void *pTarget, void *pObject, bool bRegister);

			virtual ~IKernel() { }

			virtual void Unregister(bool bClear = false) = 0;

			virtual bool LoadModule(const char *filename) = 0;

			virtual Pointer::IInterfaceOffset *CreateInterfaceOffset() = 0;

			virtual void Register(void *pObject, Pointer::IInterfaceOffset *pIO) = 0;
			virtual void Register(void *pObject, size_t nInterfaceID, intptr_t aOffset) = 0;
			virtual void Register(void *pObject, size_t nInterfaceID, bool bRegister) = 0;

			virtual void *GetInterfaceWithID(size_t nInterfaceID, size_t nIndex = 0) = 0;
			
			static inline IKernel *GetKernel(IKernel* pKernel = NULL)
			{
				static IKernel* s_pKernel;
				
				if (pKernel != NULL)
				{
					s_pKernel = pKernel;
				}
				
				return s_pKernel;
			}
			
			template <typename TClass> inline TClass * Register(TClass *pClass)
			{
				GetKernel(this);

				return pClass;
			}
		};
	}
}