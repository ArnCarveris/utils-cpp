#ifndef PLUGIN_SHAREDLIBRARY_H
#define PLUGIN_SHAREDLIBRARY_H


#include <string>
#include <stdexcept>

#if defined(WIN32)

#define WIN32_LEAN_AND_MEAN
#define VC_EXTRALEAN
#include <windows.h>

#elif defined(LINUX)

#include <dlfcn.h>

#else

#error Please implement the shared library functions for your system

#endif
				
namespace Utils
{
	namespace Plugin {


#if defined(WIN32)

		struct SharedLibrary
		{
			typedef HMODULE HandleType;

			static HandleType Load(const char *szPath)
			{
				static std::string pathWithExtension;

				pathWithExtension.clear();
				pathWithExtension += szPath;
				pathWithExtension +=  ".dll";

				HMODULE moduleHandle = ::LoadLibraryA(pathWithExtension.c_str());

				if (moduleHandle == NULL)
				{
					pathWithExtension = "Could not load: " + pathWithExtension;

					throw std::runtime_error(pathWithExtension.c_str());
				}

				return moduleHandle;
			}

			static void Unload(HandleType sharedLibraryHandle)
			{
				BOOL result = ::FreeLibrary(sharedLibraryHandle);

				if (result == FALSE)
				{
					throw std::runtime_error("Could not unload DLL");
				}
			}

			template<typename TSignature>  static TSignature *GetFunctionPointer(HandleType sharedLibraryHandle, const char *szFunctionName)
			{
				FARPROC functionAddress = ::GetProcAddress(sharedLibraryHandle, szFunctionName);

				return functionAddress ? reinterpret_cast<TSignature *>(functionAddress) : NULL;
			}

		};

#endif

#if defined(LINUX)
		struct SharedLibrary
		{
			typedef void * HandleType;

			static HandleType Load(const std::string &path)
			{
				std::string pathWithExtension = std::string("./lib") + path + ".so";

				void *sharedObject = ::dlopen(pathWithExtension.c_str(), RTLD_NOW);
				if(sharedObject == NULL)
				{
					pathWithExtension = "Could not load: " + pathWithExtension;

					throw std::runtime_error(pathWithExtension.c_str());
				}

				return sharedObject;
			}

			static void Unload(HandleType sharedLibraryHandle)
			{
				int result = ::dlclose(sharedLibraryHandle);

				if(result != 0)
				{
					throw std::runtime_error("Could not unload shared object");
				}
			}


			template<typename TSignature> static TSignature *GetFunctionPointer(HandleType sharedLibraryHandle, const std::string &functionName)
			{
				::dlerror(); 

				void *functionAddress = ::dlsym(sharedLibraryHandle, functionName.c_str());

				const char *error = ::dlerror();

				if(error != NULL)
				{
					throw std::runtime_error("Could not find exported function");
				}

				return reinterpret_cast<TSignature *>(functionAddress);
			}

		};

#endif

	}
}
#endif 
