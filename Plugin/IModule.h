#pragma once
namespace Utils
{
	namespace Plugin
	{
		struct IXKernel;
		struct IModule
		{
			virtual ~IModule() {};
			virtual bool RegisterModule(IXKernel *kernel) = 0;
		};
	}
}