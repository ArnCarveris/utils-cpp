#pragma once

#include "Object.h"
#include "../XDelegate.h"


#define OnDelegate(n, t, a, pre, post) On##n(t a){pre;for(On##n##Vector::iterator it = m_vOn##n##.begin(); it != m_vOn##n##.end(); it++)(*it)(a);post;}


#define UtilsPluginClassRegister(c) ; namespace Register { struct Entry##c : public Utils::Plugin::Class::Register::Entry<c, false> { } static aEntry##c; };// static c *g_p##c = new c

namespace Utils
{
	namespace Plugin
	{
		namespace Class
		{
			struct Bundle
			{
				typedef Delegate::X1<void, IXKernel*> OnLoadDelegate;
				typedef Delegate::X1<void, const char*> OnNameDelegate;
				typedef Delegate::X0<void> OnUnLoadDelegate;

				typedef std::vector<OnLoadDelegate> OnLoadVector;
				typedef std::vector<OnNameDelegate> OnNameVector;
				typedef std::vector<OnUnLoadDelegate> OnUnLoadVector;


				OnLoadVector	m_vOnLoad;
				OnNameVector	m_vOnName;
				OnUnLoadVector	m_vOnUnLoad;

				IXKernel *m_pKernel;

				void OnDelegate(Load, IXKernel*, pKernel,m_pKernel = pKernel,);
				void OnDelegate(Name, const char*, szName,,);
				void OnDelegate(UnLoad,,,,);


				Bundle() : m_pKernel(NULL) {}
				~Bundle()
				{
					m_vOnLoad.clear();
					m_vOnUnLoad.clear();
					m_vOnName.clear();
					m_pKernel = NULL;
				}

				template <typename TVector> typename TVector::iterator GetPushingIterator(TVector &aVector, bool bFront)
				{
					return bFront ? aVector.begin() : aVector.end();
				}

				void RegisterLoading(bool bPushFront, const OnLoadDelegate &aDelegate)
				{
					if (m_pKernel)
					{
						aDelegate(m_pKernel);
					}
					else
					{
						m_vOnLoad.insert(GetPushingIterator(m_vOnLoad, bPushFront), aDelegate);				
					}
				}

				void RegisterUnLoading(bool bPushFront, const OnUnLoadDelegate &aDelegate)
				{
					m_vOnUnLoad.insert( GetPushingIterator(m_vOnUnLoad, bPushFront), aDelegate);
				}

				void RegisterNaming(bool bPushFront, const OnNameDelegate &aDelegate)
				{
					m_vOnName.insert( GetPushingIterator(m_vOnName, bPushFront), aDelegate);
				}

				template <typename TClass> TClass *Register(bool bPushFront = false, TClass *p = NULL)
				{
					if (!p)
					{
						p = new TClass();
					}

					RegisterLoading(bPushFront, OnLoadDelegate::FromMethod<TClass, TClass::Register>(p));	
					
					return p;
				}
				

			};

			extern Bundle aBundle;

			namespace Register
			{
				template <typename TClass, bool bPushFront> class Entry
				{
				public:
					Entry()
					{
						aBundle.Register<TClass>(bPushFront);
					}
				};
			}
			
			template <typename TClass, bool bDelete = false> class Item : public Object<TClass>
			{
			protected:
				void RegisterBundleLoading(bool bPushFront)
				{
					aBundle.Register<TClass>(bPushFront, (TClass*)this);	

#if 0 // FEEFEE2 for 'DXRenderer'
					aBundle.RegisterUnLoading(bPushFront, Bundle::OnUnLoadDelegate::FromMethod<TClass, TClass::UnRegister>((TClass*)this));
#endif
				}

				void RegisterBundleNaming(bool bPushFront, const Bundle::OnNameDelegate &aDelegate)
				{
					aBundle.RegisterNaming(bPushFront, aDelegate);
				}

			public:

				Item()
				{

				}

				Item(bool bPushFront)
				{
					RegisterBundleLoading(bPushFront);
				}


				virtual void Register(IXKernel *pKernel)
				{
					RegisterObject(pKernel);
				}
	
				virtual void UnRegister()
				{
					if (bDelete)
					{
						delete this;
					}
				}
			};

			template <typename TClass, bool bDelete = false> class T : public Item<TClass, bDelete>
			{
			public:
				typedef T<TClass, bDelete> TType;
			};	
		}
	}
}

#undef OnDelegate
