
#pragma once

#include "../Base.h"
#include "IXKernel.h"
#include "XModule.h"

#include <map>
#include <set>
#include <stdexcept>

#include <assert.h>


namespace Utils
{
	namespace Plugin
	{	
		namespace Register
		{
			template <typename V, typename T, bool bDelete, bool bLoop> void InVector(V *pVector, T& rObject, bool bRegister)
			{
				if (bRegister)
				{
					pVector->push_back(rObject);
				}
				else do
				{
					V::iterator it;

					if (rObject)
					{
						it = std::find(pVector->begin(), pVector->end(), rObject);
					}
					else
					{
						it = pVector->begin();
					}

					if (it == pVector->end())
					{
						return;
					}

					pVector->erase(it);

					if (bDelete)
					{
						delete *it;
					}

				} while(bLoop && !rObject);
			}
			
			template <typename V, typename T, bool bDelete> void InVector(V *pVector, T& rObject, bool bRegister)
			{
				InVector<V, T, bDelete, true>(pVector, rObject, bRegister);
			}
			template <typename V, typename T, bool bDelete> void InVectorThunk(void *pVector, void* pObject, bool bRegister)
			{
				InVector<V,T,bDelete>((V*)pVector, (T&)pObject, bRegister);
			}



			template <typename T, bool bDelete> void InVector(std::vector<T> *pVector, T& rObject, bool bRegister)
			{
				InVector<std::vector<T>, T, bDelete> (pVector, rObject, bRegister);
			}
			template <typename T, bool bDelete> void InVectorThunk(void *pVector, void* pObject, bool bRegister)
			{
				InVectorThunk<std::vector<T>, T, bDelete> (pVector, pObject, bRegister);
			}

			template <typename T> void InPointer(typename T::TPointer pObject, bool bRegister)
			{
				if (bRegister)
				{
					pObject->Retain();
				}
				else if (pObject->Release())
				{
					delete pObject;
				}
			}

			template <typename T> void As(typename T::TVector *pVector,typename T::TPointer pObject, bool bRegister)
			{
				do
				{
					T::TPointer rObject = pObject;

					if (!pObject)
					{
						if (pVector->size() > 0)
						{
							rObject = pVector->front();
						}
						else return;
					}

					InVector<T::TVector, T::TPointer, false, false> (pVector, rObject, bRegister);

					InPointer<T>(rObject, bRegister);
					
				} while(!pObject);
			}

			template <typename T> void AsThunk(void *pVector, void* pObject, bool bRegister)
			{
				As<T>((T::TVector*)pVector, (T::TPointer)pObject, bRegister);
			}
		}

		namespace Accessor
		{
			
			template <typename V, typename T> T InVector(V *pVector, size_t nIndex)
			{
				return nIndex < pVector->size() ? pVector->at(nIndex) : (T)(0);
			}
			
			template <typename V, typename T> void *InVector(void *pVector, size_t nIndex)
			{
				return nIndex == (size_t)(-1) ? pVector : (void*)InVector<V, T>((V *)pVector, nIndex);
			}

			template <typename T> T InVector(std::vector<T> *pVector, size_t nIndex)
			{
				return InVector<std::vector<T>, T> (pVector, nIndex);
			}
			
			template <typename T> void *InVector(void *pVector, size_t nIndex)
			{
				return InVector<std::vector<T>, T> (pVector, nIndex);
			}


		}

		template <typename TModule = CXModule> class CXKernel : public IXKernel{

		public:
			typedef CXKernel TXKernel;


			template <typename T> bool AddInterfaceAccessor(void *pTarget, InterfaceAccessorThunk hThunk)
			{
				return AddInterfaceAccessorWithID(T::GetID(), pTarget, hThunk);
			}
			template <typename T> bool AddInterfaceRegister(void *pTarget, InterfaceRegisterThunk hThunk)
			{
				return AddInterfaceRegisterWithID(T::GetID(), pTarget, hThunk);
			}


			template <typename T> bool AddInterfaceAccessor(InterfaceAccessorThunk hThunk)
			{
				return AddInterfaceAccessor<T>(this, hThunk);
			}
			template <typename T> bool AddInterfaceRegister(InterfaceRegisterThunk hThunk)
			{
				return AddInterfaceRegister<T>(this, hThunk);
			}

			template <typename TID, typename V, typename T, bool bDelete> bool AddInterfaceRegister(V *pVector)
			{
				return AddInterfaceRegister<TID>((void*)pVector, &Register::InVectorThunk<V, T, bDelete>);
			}
			template <typename TID, typename V, typename T> bool AddInterfaceAccessor(V *pVector)
			{
				return AddInterfaceAccessor<TID>((void*)pVector, &Accessor::InVector<V, T>);
			}
			template <typename TID, typename V> V *GetVector()
			{
				return (V *)GetInterfaceWithID(TID::GetID(), -1);
			}

			template <typename TID, typename T, bool bDelete> bool AddInterfaceRegister(std::vector<T> *pVector)
			{
				return AddInterfaceRegister<TID>((void*)pVector, &Register::InVectorThunk<T, bDelete>);
			}
			template <typename TID, typename T> bool AddInterfaceAccessor(std::vector<T> *pVector)
			{
				return AddInterfaceAccessor<TID>((void*)pVector, &Accessor::InVector<T>);
			}
			template <typename TID, typename T> std::vector<T> *GetVector()
			{
				return GetVector<TID, std::vector<T> >();
			}
			

			template <typename T> bool AddInterfaceRegister(std::vector<T*>  *pVector)
			{
				return AddInterfaceRegister<T, T*, true> (pVector);
			}
			template <typename T> bool AddInterfaceAccessor(std::vector<T*>  *pVector)
			{
				return AddInterfaceAccessor<T, T*> (pVector);
			}
			template <typename T> bool AddInterface(std::vector<T*>  *pVector)
			{
				return AddInterfaceRegister<T>(pVector) && AddInterfaceAccessor<T>(pVector);
			}

			template <typename T> std::vector<T*>  *GetVector()
			{
				return GetVector<T, T*>();
			}
			

			template <typename T> bool AddInterfaceRegister(typename T::TVector *pVector)
			{
				return AddInterfaceRegister<T>((void*)pVector, &Register::AsThunk<T>);
			}
			template <typename T> bool AddInterfaceAccessor(std::vector< typename T::TPointer >  *pVector)
			{
				return AddInterfaceAccessor<T, T::TPointer > (pVector);
			}
			template <typename T> bool AddInterface(typename T::TVector *pVector)
			{
				return AddInterfaceRegister<T>(pVector) && AddInterfaceAccessor<T>(pVector);
			}
			template <typename T> std::vector< Pointer::Void*>  *GetVector()
			{
				return GetVector<T, Pointer::Void *>();
			}

			template <typename T> inline T* CreateObject()
			{
				T *pT = new T();

				pT->RegisterObject(this);

				return pT;
			}

			bool AddInterfaceRegisterWithID(size_t nInterfaceID, void *pTarget, InterfaceRegisterThunk hThunk)
			{
				assert(nInterfaceID);

				if (this->interfaceRegisterMap.find(nInterfaceID) == this->interfaceRegisterMap.end()) {

					this->interfaceRegisterMap.insert(std::make_pair(nInterfaceID, std::make_pair(pTarget, hThunk)));

					return true;
				}

				return false;
			}

			bool AddInterfaceAccessorWithID(size_t nInterfaceID, void *pTarget, InterfaceAccessorThunk hThunk)
			{
				assert(nInterfaceID);

				if (this->interfaceAccessMap.find(nInterfaceID) == this->interfaceAccessMap.end()) {

					this->interfaceAccessMap.insert(std::make_pair(nInterfaceID, std::make_pair(pTarget, hThunk)));

					return true;
				}

				return false;
			}

			void *GetInterfaceWithID(size_t nInterfaceID, size_t nIndex = 0)
			{
				assert(nInterfaceID);

				return GetInterface(interfaceAccessMap[nInterfaceID], nIndex);
			}
			
			Pointer::IInterfaceOffset *CreateInterfaceOffset()
			{
				return Pointer::IInterfaceOffset::CurrentCreator()();
			}

			void Register(void *pObject, Pointer::IInterfaceOffset *pIO)
			{
				Pointer::Void *p = Pointer::Void::Include(&interfacePointerSet, pObject, false);
				Pointer::Void::Set(&interfacePointerSet);

				p->RegisterIfNeeded(pIO);

				for(size_t r = 1, nID; (nID = pIO->GetNextKey(r)); r = 0)
				{
					Register(p,nID, true);
				}
				
				Pointer::Void::Set(NULL, true);
			}

			void Register(void *pObject, size_t nInterfaceID, int aOffset)
			{
				Pointer::Void *p = Pointer::Void::Include(&interfacePointerSet, pObject, false);

				p->Register(nInterfaceID, aOffset);

				Register(p,nInterfaceID, true, true, true);
			}

			void Register(void *pObject, size_t nInterfaceID, bool bRegister)
			{
				Register(pObject, nInterfaceID, bRegister, !bRegister, !bRegister);
			}
		
			bool LoadModule(const char * szFilename) 
			{
				if (this->loadedModules.find(szFilename) == this->loadedModules.end())
				{
					TModule cModule(szFilename);

					if (cModule.RegisterModule(this))
					{
						loadedModules.insert(ModuleMap::value_type(szFilename, cModule));

						return true;
					}
				}

				return false;
			}

			void Unregister(bool bClear = false)
			{
				Pointer::Void::Set(&interfacePointerSet);

				for iterator_each(InterfaceRegisterMap, interfaceRegisterMap, it)
				{
					Register(it->second, NULL, false);
				}
				
				if (bClear)
				{
					interfaceRegisterMap.clear();
				}

				Pointer::Void::Set(NULL, true);
			}

			CXKernel()
			{
				Pointer::IInterfaceOffset::CurrentCreator(&Pointer::CInterfaceOffset::Creator);
			}

			virtual ~CXKernel()
			{
				Unregister();
			}

		private:
			typedef std::pair<void*, InterfaceAccessorThunk> AccessorPair;
			typedef std::pair<void*, InterfaceRegisterThunk> RegisterPair;

			typedef std::map<std::string, TModule> ModuleMap;
			typedef std::map<size_t, AccessorPair> InterfaceAccessMap;
			typedef std::map<size_t, RegisterPair> InterfaceRegisterMap;

			typedef Pointer::Void::XSet InterfacePointerSet;

			InterfaceAccessMap			interfaceAccessMap;
			InterfaceRegisterMap		interfaceRegisterMap;
			ModuleMap					loadedModules;
			InterfacePointerSet			interfacePointerSet;

			
			void Register(void *pObject, size_t nInterfaceID, bool bRegister, bool bDoSet, bool bUnSet)
			{
				assert(nInterfaceID);

				Register(interfaceRegisterMap[nInterfaceID], pObject, bRegister, bDoSet, bUnSet);
			}

			void Register(const RegisterPair & rPair, void *pObject, bool bRegister)
			{
				rPair.second(rPair.first, pObject, bRegister);
			}
			void Register(const RegisterPair & rPair, void *pObject, bool bRegister, bool bDoSet, bool bUnSet)
			{
				if (bDoSet) Pointer::Void::Set(&interfacePointerSet);
				Register(rPair, pObject, bRegister);
				if (bUnSet) Pointer::Void::Set(NULL, true);
			}

			void* GetInterface(const AccessorPair & rPair, size_t nIndex)
			{
				return rPair.second(rPair.first, nIndex);
			}
		};

	}
}