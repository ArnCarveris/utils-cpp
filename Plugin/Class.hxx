
#include "Class.h"

namespace Utils
{
	namespace Plugin
	{
		namespace Class
		{
			Bundle aBundle;
		}
	}
}

#ifdef MODULE_API
MODULE_FUNCTION void OnModuleLoad(Utils::Plugin::IXKernel *pKernel)
{
	Utils::Plugin::Class::aBundle.OnLoad(pKernel);
} 
MODULE_FUNCTION void OnModuleUnLoad()
{
	Utils::Plugin::Class::aBundle.OnUnLoad();
}

MODULE_FUNCTION void SetModuleName(const char *szName)
{
	Utils::Plugin::Class::aBundle.OnName(szName);
}
#endif
