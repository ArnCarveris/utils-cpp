#pragma once

#include "IXKernel.h"



namespace Utils
{
	namespace Plugin
	{
		namespace Class
		{
			template <typename TClass> class Object 
			{
			public:
				void RegisterObject(IXKernel *pKernel)
				{
					Pointer::IInterfaceOffset **pIO = GetInterfaceOffsetAddress();

					size_t *pAddress[] =
					{
						(size_t*)((TClass*)this),
						(size_t*)((void*)this)
						
					};

					if ((*pIO) == NULL)
					{
						Interface::UpdateInterfaceOffsets(pAddress[0], pAddress[1], (*pIO = pKernel->CreateInterfaceOffset()), 0, 2);
					}

					pKernel->Register((void*)pAddress[0], *pIO);
				}
				

				Pointer::IInterfaceOffset **GetInterfaceOffsetAddress()
				{
					static Pointer::IInterfaceOffset *s_pIO = NULL;

					return &s_pIO;
				}
			};
		}
	}
}