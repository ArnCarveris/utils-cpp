#ifndef _UTILS_CPP_XITEM_H_
#define _UTILS_CPP_XITEM_H_

#include "Base.h"

namespace Utils
{
	class XItem
	{
	public:
		
		DeclareAccessor(ID, unsigned int, iID, protected);
		DeclareAccessor(Name, const char *, szName, protected);

	};
}

#endif