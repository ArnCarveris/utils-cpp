#include "XBit.h"
#include <stdio.h>

namespace Utils
{
	namespace Bit
	{
		unsigned int GetCount(unsigned int n)
		{
			/*
			static const int l = 0x1u << 16;
			static unsigned int i = 0;
			static char data[l];

			for (unsigned int c = 0, j = 0; i < l; i++, c = 0, j = i) // Calls Only Once Due Static 'i'
			{
				while (j > 0)
				{
					c++;// = j & 0x1u;
					j >>= 1;
				}

				data[i] = c;
			}

			return data[n & 0xffffu] + data[(n >> 16) & 0xffffu];
				
			//*/

			//*
			int c = 0;

			while (n > 0)
			{
				c++;
				n >>= 1;
			}
			
			return c;
			//*/
		}

		void Print(unsigned int n)
		{
			while (n > 0)
			{
				printf("%c", n % 2 ? '1' : '0');
				n >>= 1;
			}
		}
	}
}