/*
	(c) Sergey Ryazanov (http://home.onego.ru/~ryazanov)

	Fast delegate compatible with C++ Standard.

	Changed code style by ArnCarveris, implemented sources Handler
*/
#ifndef SRUTIL_EVENT_INCLUDED
#define SRUTIL_EVENT_INCLUDED

#include "Base.h"

namespace Utils
{
	namespace Event
	{
		XT(Sink) class XSource;

		XT(Sink) class XBinder
		{
		public:
			typedef XSource<TSink> TSource;

			XBinder() : sink(TSink())
			{
				next = prev = this;
			}

			~XBinder()
			{
				UnBind();
			}

			void Bind(const TSource& source, TSink sink)
			{
				UnBind();

				AttachAfter(&source.list_head);

				this->sink = sink;
			}

			void UnBind()
			{
				if (prev) prev->next = next;
				if (next) next->prev = prev;
				next = prev = NULL;
			}

		private:

			friend class TSource;

			/*
			XBinder(XBinder const&);
			XBinder const& operator=(XBinder const&);
			*/

			void AttachAfter(XBinder* that)
			{
				next = that->next;
				next->prev = this;
				that->next = this;
				prev = that;
			}

			size_t		id;
			XBinder*	prev;
			XBinder*	next;
			TSink		sink;
		};

		XT(Sink) class XSource
		{
		public:
			typedef XBinder<TSink> TBinder;

			XSource() : count(0){}

			~XSource()
			{
				UnBind();
			}

			size_t Bind(TSink sink, TBinder* bndr = NULL)
			{

				if (bndr == NULL)
				{
					bndr = push();
				}

				bndr->AttachAfter(&list_head);
				bndr->sink = sink;

				return (bndr->id = count++);
			}

			void UnBind()
			{
				while (stack.size() > 0)
				{
					pop(true);
				}

				count = 0;
			}

			void UnBind(size_t id)
			{
				TBinder* current = list_head.next;
				
				size_t i = count;

				while (current != &list_head && i-- > 0)
				{
					if (current->id == id)
					{
						current->UnBind();

						TBinder *bndr = NULL;
						size_t idx = 0;
						for(size_t j = 0, l = stack.size(); j < l; j++)
						{
							bndr = stack[j];

							if (bndr->id != id)
							{
								bndr = NULL;
							}
							else
							{
								idx = j;
								break;
							}
						}

						if (bndr)
						{
							stack.erase(stack.begin() + idx);

							delete bndr;
							bndr = NULL;
						}

						break;
					}
					else
					{
						current = current->next;
					}
				}
			}

			XT(Invoker) void Emit(TInvoker const& invoker)
			{
				TBinder* current = list_head.next;
				
				size_t i = count;

				while (current != &list_head && i-- > 0)
				{
					if (current->sink)
					{
						TBinder bookmark;
						bookmark.AttachAfter(current);

						invoker(current->sink); // *current may be excluded from list, but mookmark will always keep valid

						current = bookmark.next;
					}
					else
					{
						current = current->next;
					}
				}
			}

		private:

			TBinder *push()
			{
				TBinder *b = new TBinder();

				stack.push_back(b);

				return stack.back();
			}

			TBinder * pop(bool bDelete)
			{
				TBinder *b = stack.back();
				
				stack.pop_back();
			
				b->UnBind();

				if (bDelete)
				{
					delete b;

					b = NULL;
				}
				
				return b;
			}

			size_t count;

			mutable std::vector<TBinder*> stack;
			mutable TBinder list_head;
			friend class TBinder;
		};

		XT(Sink) class XHandler
		{
		public:
			typedef XBinder <TSink> TBinder;
			typedef XSource <TSink> TSource;

			XHandler() { }

			~XHandler()
			{
				UnBind();
			}

			size_t Bind(const char *source, TSink sink, TBinder * binder = NULL)
			{
				return GetSource(source, true)->Bind(sink, binder);
			}

			void UnBind(const char *source)
			{
				GetSource(source, true)->UnBind();
			}

			void UnBind(const char *source, size_t id)
			{
				GetSource(source, true)->UnBind(id);
			}

			void UnBind()
			{
				for (int i = 0, l = (int)sources.GetCount(); i < l; i++)
				{
					UnBind(sources.GetKeyAtIndex(i));
				}

				sources.Clear();
			}

			XT(Invoker) void Emit(const char *source, TInvoker const& invoker)
			{
				TSource *src = GetSource(source);

				if (src != NULL)
				{
					src->Emit(invoker);
				}
			}

			TSource *GetSource(const char *source, bool bCreateIfNeeded = false)
			{
				if (bCreateIfNeeded && sources.Has(source) == false)
				{
					TSource src;

					sources.Add(source, src);
				}

				return sources.Get(source);
			}

		private:

			XDictionary<const char*, TSource> sources;
		};

		XTN(Delegate), typename TInvoker> struct XBasic
		{
			TInvoker			hInvoker;
			XSource<TDelegate>	hSource;
			
			XBasic				()						{ }
			XBasic				(TInvoker	&invoker)	{ hInvoker = invoker;}
			
			~XBasic				()						{ UnBind(); }
			
			void UnBind			()						{ hSource.UnBind(); }
			void UnBind			(size_t id)				{ hSource.UnBind(id); }

			void Emit			()						{ hSource.Emit(hInvoker); }
			void Emit			(TInvoker	&invoker)	{ hInvoker = invoker; Emit(); }
			void DoEmit			(TInvoker	&invoker)	{ hSource.Emit(invoker); }
			size_t Bind			(TDelegate &hDelegate)	{ return hSource.Bind(hDelegate); }
		};
		

		XTN(Delegate), typename TInvoker> struct XMapped
		{
			TInvoker			hInvoker;
			XHandler<TDelegate>	hHandler;
			
			XMapped				()											{ }
			XMapped				(TInvoker	&invoker)						{ hInvoker = invoker;}
			
			~XMapped			()											{ UnBind(); }
			
			void UnBind			()											{ hHandler.UnBind(); }

			void Emit			(const char *szName)						{ hHandler.Emit(szName, hInvoker); }
			void Emit			(const char *szName, TInvoker &invoker)		{ hInvoker = invoker; Emit(szName); }
			void DoEmit			(const char *szName, TInvoker &invoker)		{ hHandler.Emit(szName, invoker); }
			size_t Bind			(const char *szName, TDelegate &hDelegate)	{ return hHandler.Bind(szName, hDelegate); }
			void UnBind			(const char *szName)						{ hHandler.UnBind(szName); }
			void UnBind			(const char *szName, size_t id)				{ hHandler.UnBind(szName, id); }
		};
	}
}


#define typedef_utils_event_basic(_delegate_, _name_) typedef Utils::Event::XBasic<_delegate_, _delegate_::TInvoker> _name_
#define typedef_utils_event_mapped(_delegate_, _name_) typedef Utils::Event::XMapped<_delegate_, _delegate_::TInvoker> _name_
#define typedef_utils_event_x(_type_, _name_, _pre_, _post_) typedef_utils_event(Utils::Delegate::X##_type_, _name_, _pre_, _post_)
#define typedef_utils_event(_type_, _name_, _pre_, _post_) namespace _name_ \
{_pre_\
	typedef _type_											TDelegate;	\
	typedef TDelegate::TInvoker								TInvoker;	\
	typedef Utils::Event::XBasic < TDelegate, TInvoker>		TBasic;		\
	typedef Utils::Event::XMapped < TDelegate, TInvoker>	TMapped;	\
_post_}

#endif// SRUTIL_EVENT_INCLUDED
