#pragma once

#include <algorithm>
#include <set>
#include <map>

namespace Utils
{
	namespace Pointer
	{
		static bool IsMagical(size_t nPtr)
		{
			static size_t aMagicalPtr[] = {0xbadf00d, 0xdeadbeef, 0xabababab, 0xbdbdbdbd, 0xfdfdfdfd, 0xcdcdcdcd, 0xdddddddd, 0xfeeefeee};
			static size_t nMagicalPtr = sizeof(aMagicalPtr) / sizeof(aMagicalPtr[0]);

			for(size_t i = 0; i < nMagicalPtr; i++) if (aMagicalPtr[i] == nPtr)
			{
				return true;
			}

			return false;
		}
		struct IInterfaceOffset
		{
			typedef IInterfaceOffset*(*CreatorThunk)();

			virtual ~IInterfaceOffset() { }
			virtual bool Add(size_t nInterfaceID, intptr_t aOffset, bool bSet = false) = 0;
			virtual void *Get(void *pBase, size_t nInterfaceID) = 0;
			virtual void Clear() = 0;
			virtual size_t Size() = 0;
			virtual size_t GetNextKey(bool bReset = false) = 0;
			static CreatorThunk CurrentCreator(CreatorThunk pThunk = NULL)
			{
				static CreatorThunk s_pThunk = NULL;
			
				if (pThunk != NULL)
				{
					s_pThunk = pThunk;
				}

				return s_pThunk;
			}
		};

		struct ISet
		{
			virtual ~ISet() { } 
			virtual void *Include(void*pObject, bool bUseCounter = true) = 0;
			virtual bool Exclude(void*pObject) = 0;
			virtual void *Find(void*pObject, void *pPredicate) = 0;
		};

		class CInterfaceOffset : public IInterfaceOffset
		{
		public:
			CInterfaceOffset() { }

			~CInterfaceOffset()
			{
				Clear();
			}

			void Clear()
			{
				m_Map.clear();
			}

			bool Add(size_t nInterfaceID, intptr_t aOffset, bool bSet = false)
			{
				if (!nInterfaceID)
				{
					return false;
				}
				
				if (m_Map.size() > 0 && m_Map.find(nInterfaceID) != m_Map.end())
				{
					if (bSet)
					{
						m_Map[nInterfaceID] = aOffset;
					}
					
					return bSet;
				}

				m_Map.insert(std::make_pair(nInterfaceID, aOffset));

				return true;
			}

			void *Get(void *pBase, size_t nInterfaceID)
			{
				if (!pBase || !nInterfaceID || IsMagical((size_t)pBase))
				{
					return NULL;
				}

				InterfaceOffsetMap::iterator it = m_Map.find(nInterfaceID);

				if (it == m_Map.end() || IsMagical((size_t)(pBase = (void*)((intptr_t*)pBase + it->second))))
				{
					return NULL;
				}

				return pBase;
			}

			static IInterfaceOffset *Creator()
			{
				return new CInterfaceOffset();
			}

			size_t Size()
			{
				return m_Map.size();
			}

			size_t GetNextKey(bool bReset = false)
			{
				static InterfaceOffsetMap::iterator it;
				
				if (bReset)
				{
					it = m_Map.begin();
				}
				else
				{
					it++;
				}

				return it == m_Map.end() ? 0 : it->first;
			}
		private:
			typedef std::map<size_t, intptr_t> InterfaceOffsetMap;

			InterfaceOffsetMap m_Map;
		};

		class CReferenceCounter
		{
		public:
			CReferenceCounter() : nCount(0) { }

			void Reset()
			{
				nCount = 0;
			}

			void Retain()
			{
				nCount++;
			}

			bool Release()
			{
				return --nCount == 0;
			}

		private:

			size_t nCount;
		};

		template <typename T = void> class Base
		{
		public:
			typedef Base TBase;
			typedef Base *TPtrBase;
			
			Base() : m_pT(0), m_pRC(0), m_pIO(0), m_bExternalIO(0)
			{
				Retain();
			}
			Base(T *pT) : m_pT(pT), m_pRC(0), m_pIO(0), m_bExternalIO(0)
			{
				Retain();
			}

			Base(const Base<T> &refAs) : m_pT(refAs.m_pT), m_pRC(refAs.m_pRC), m_pIO(refAs.m_pIO), m_bExternalIO(refAs.m_bExternalIO)
			{
				Retain();
			}

			virtual ~Base()
			{
				Release();
			}

			T* operator->()
			{
				return m_pT;
			}
			Base<T> & operator = (const Base<T> &refAs)
			{
				if (this != &refAs)
				{
					Assign(refAs.m_pT);
				}

				return *this;
			}

			Base<T> & operator = (const T *pT)
			{
				Assign(pT);
				
				return *this;
			}


			void Reset(bool bZero = false)
			{
				if (!m_pRC)
				{
					m_pRC = new CReferenceCounter();
				}
				else if (bZero)
				{
					m_pRC->Reset();
				}
			}
			
			void Retain()
			{
				Reset();

				m_pRC->Retain();
			}
			
			bool Release()
			{

				if (!m_pRC || !m_pRC->Release())
				{
					return false;
				}
				

				delete m_pRC;
				m_pRC = NULL;

				if (m_pIO)
				{
					if (!m_bExternalIO)
					{
						delete m_pIO;
					}

					m_pIO = NULL;
				}

				if (m_pT)
				{
					Base::Exclude(m_pT);

					delete m_pT;
					m_pT = NULL;
				}

				return true;
			}

			struct Predicate
			{
				Predicate(T *pT) : m_pT(pT) { }

				bool operator()(const Base<T> & rBase)
				{
					return rBase.m_pT == m_pT;
				}

				bool operator()(const Base<T> * pBase)
				{
					return pBase->m_pT == m_pT;
				}

			private:
				T *m_pT;
			};

			
			class XSet : public ISet
			{
			public:

				XSet () { }
				virtual ~XSet() { m_aSet.clear();}

				void *Include(void*pObject, bool bUseCounter = true)
				{
					TPtrBase p = NULL;

					TSet::iterator it;

					Find(pObject, &it);

					T *pT = (T *)pObject;

					if (it == m_aSet.end())
					{
						p = new TBase(pT);

						if (!bUseCounter)
						{
							p->Reset(true);
						}

						m_aSet.insert(m_aSet.begin(),(TPtrBase) p);
					}
					else
					{
						p = *it;
						
						if (bUseCounter)
						{
							p->Retain();
						}
					}

					return (void*)p;
				}

				bool Exclude(void*pObject)
				{
					TSet::iterator it;

					Find(pObject, &it);

					if (it == m_aSet.end())
					{
						return false;
					}

					m_aSet.erase(it);

					return true;
				}

				void *Find(void*pObject, void *pPredicate)
				{
					*((TSet::iterator*)pPredicate) = std::find_if(m_aSet.begin(), m_aSet.end(), Predicate((T*)pObject));

					return pPredicate;
				}

			private:
				typedef std::set<TPtrBase> TSet;

				TSet m_aSet;
			};

			static ISet *Set(ISet *pSet = NULL, bool bForce = false)
			{
				static ISet *s_pSet = NULL;

				if (bForce || s_pSet != NULL)
				{
					s_pSet = pSet;
				}

				return s_pSet;
			}

			
			static TPtrBase Include(ISet *pSet, T *pT, bool bUseCounter = true)
			{
				return pSet ? (TPtrBase)pSet->Include((void*)pT, bUseCounter) : NULL;
			}

			static bool Exclude(ISet *pSet,T *pT)
			{
				return pSet && pSet->Exclude((void*)pT);
			}

			
			static TPtrBase Include(T *pT, bool bUseCounter = true)
			{
				return Include(Set(), pT, bUseCounter);
			}

			static bool Exclude(T *pT)
			{
				return Exclude(Set(), pT);
			}
			
			void RegisterIfNeeded(IInterfaceOffset *pIO = NULL)
			{
				if (!m_pIO)
				{
					m_pIO = (m_bExternalIO = pIO != NULL) ? pIO : IInterfaceOffset::CurrentCreator()();

				}
			}

			void Register(size_t nInterfaceID, intptr_t aOffset)
			{
				RegisterIfNeeded();

				m_pIO->Add(nInterfaceID, aOffset);
			}

			void *Get(size_t nInterfaceID)
			{
				return m_pIO ? m_pIO->Get(m_pT, nInterfaceID) : NULL;
			}

			template <typename TInterface> TInterface *Get(size_t nInterfaceID)
			{
				return (TInterface*)Get(nInterfaceID);
			}

			template <typename TInterface> TInterface *Get()
			{
				return (TInterface*)Get(TInterface::GetID());
			}

			IInterfaceOffset *GetInterfaceOffsets()
			{
				return m_pIO;
			}
		protected:

			bool Assign(T *pT)
			{	
				if (m_pT == pT)
				{
					return false;
				}

				Retain(pT);

				return true;
			}

			void Retain(T *pT)
			{
				Release();
				
				m_pT = pT;

				Retain();
			}


			T *m_pT;
			CReferenceCounter* m_pRC;
			IInterfaceOffset *m_pIO;
			bool m_bExternalIO;
		};

		typedef Base<void> Void;

		template <typename T> T *Get(void *p)
		{
			return p ? ((Void*)p)->Get<T>() : (T*)p;
		}

		template <typename T> class As: public Base<T>
		{
		public:

			T& operator*()
			{
				return *m_pT;
			}
		};
	}
}