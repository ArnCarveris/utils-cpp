#pragma once

#include <stdarg.h>

#define va_param_get() _va_param_first_
#define va_list_get() _va_param_list_

#define va_param_set() va_param_get(), va_list_get()
#define va_param_set_ex(_ex_) va_param_get(), _ex_ va_list_get()

#define va_param_ref(_type_) _type_ va_param_set_ex(va_list &)
#define va_param(_type_) _type_ va_param_get(), ...

#define va_for(_type_, _name_, _condition_, _afterthought_) for (_type_ _name_ = va_param_get(); _name_ != NULL _condition_ ; _name_ = (*(_type_ *)(((va_list_get()) += sizeof(_type_)) - sizeof(_type_))), _afterthought_ )
#define va_foreach(_type_, _name_) for (_type_ _name_ = va_param_get(); _name_ != NULL; _name_ = (*(_type_ *)(((va_list_get()) += sizeof(_type_)) - sizeof(_type_))))

#define va_body(_type_, _block_) va_start(va_list_get(), va_param_get()); {_block_} va_end(va_list_get())
#define va_block(_type_, _block_) {va_list va_list_get(); va_body(_type_, _block_);}

#define va_process(_type_, _name_, _condition_, _afterthought_, _body_) va_block(_type_, va_for(_type_, _name_, _condition_, _afterthought_) _body_)
#define va_process_each(_type_, _name_, _body_) va_block(_type_, {va_foreach(_type_,  _name_) _body_} )

#define va_vector_set(_type_, _vector_) va_foreach(_type_, _vector_item_) _vector_.push_back(_vector_item_)
#define va_vector_new(_type_, _vector_) std::vector<_type_> _vector_;va_vector_set(_type_, _vector_)

#define va_vector_array_new(_type_, _vector_, _array_, _count_) va_vector_array_new_ex(_type_, _type_, _vector_, _array_, _count_,(*va_value_dst) = (*va_value_src);)
#define va_vector_array_new_ex(_type_, _type_dest_ , _vector_, _array_, _count_, _block_) _count_ = _vector_.size();_array_ = new _type_dest_ [_count_];{_type_ * va_value_src=NULL; _type_dest_ * va_value_dst=NULL; for(size_t i = 0; i < _count_; i++) {{va_value_src=&_vector_[i];va_value_dst=&_array_[i];}_block_}};_vector_.clear()

#define va_vector_array_get(_type_, _array_, _count_) va_vector_new(_type_,_params_vector_);va_vector_array_new(_type_, _params_vector_, _array_, _count_)
#define va_vector_array_get_ex(_type_, _type_dest_ , _array_, _count_, _block_)	va_vector_new(_type_,_params_vector_);va_vector_array_new_ex(_type_, _type_dest_ , _params_vector_, _array_, _count_, _block_)

#define va_list_array_set(_type_, _array_, _count_) {va_vector_array_get(_type_, _array_, _count_);}
#define va_list_array_set_ex(_type_, _type_dest_ , _array_, _count_, _block_) {va_vector_array_get_ex(_type_, _type_dest_ , _array_, _count_, _block_);}

#define va_list_array_new(_type_, _array_, _count_) size_t _count_;_type_ * _array_;va_list_array_set(_type_, _array_, _count_)
#define va_list_array_new_ex(_type_, _type_dest_ , _array_, _count_, _block_) size_t _count_;_type_ * _array_;va_list_array_set_ex(_type_, _type_dest_ , _array_, _count_, _block_)

#define va_array_set(_type_, _array_, _count_) va_block(_type_, va_list_array_set(_type_, _array_, _count_))
#define va_array_set_ex(_type_, _type_dest_ , _array_, _count_, _block_) va_block(_type_, va_list_array_set_ex(_type_, _type_dest_ , _array_, _count_, _block_))

#define va_array_new(_type_, _array_, _count_) size_t _count_;_type_ * _array_;va_block(_type_, va_list_array_set(_type_, _array_, _count_))
#define va_array_new_ex(_type_, _type_dest_ , _array_, _count_, _block_) size_t _count_;_type_dest_  * _array_;va_block(_type_, va_list_array_set_ex(_type_, _type_dest_ , _array_, _count_, _block_))

#define va_param_pass_function_ex(_type_, _name_,_args_) _name_ (_args_ _(va_param(_type_)))
#define va_param_pass_function( _type_, _name_) _name_ (va_param(_type_)) 

#define va_param_pass_block_ex(_type_, _name_, _vars_) va_block(_type_, _name_(_vars_ _(va_param_set());))
#define va_param_pass_block(_type_, _name_) va_block(_type_, _name_(va_param_set());)

#define va_param_pass(_type_, _name_) va_param_pass_function(_type_, _name_) { va_param_pass_block(_type_, _name_); }
#define va_param_pass_ex(_type_, _name_, _args_, _vars_) va_param_pass_function_ex(_type_, _name_, _args_) { va_param_pass_block_ex(_type_, _name_, _vars_);}

#define va_param_pass_value(_type_, _name_, _return_type_) va_param_pass_function(_type_, _name_) { _return_type_ value; va_param_pass_block(_type_, value=_name_); return value;}
#define va_param_pass_value_ex(_type_, _name_, _args_, _vars_) va_param_pass_function_ex(_type_, _name_, _args_) { _return_type_ value; va_param_pass_block_ex(_type_, value=_name_, _vars_); return value;}

#define va_function_ref(_type_, _return_type_, _name_) _return_type_ _name_ (va_param_ref(_type_))
#define va_function_ref_ex(_type_, _return_type_, _name_, _args_) _return_type_ _name_ (_args_ _(va_param_ref(_type_)))

#define va_method(_type_, _return_type_, _name_) _return_type_ va_param_pass(_type_, _name_);va_function_ref(_type_, _return_type_, _name_)
#define va_method_ex(_type_, _return_type_, _name_, _args_, _vars_) _return_type_ va_param_pass_ex(_type_, _name_, _args_, _vars_);va_function_ref_ex(_type_, _return_type_, _name_, _args_)

#define va_function(_type_, _return_type_, _name_) _return_type_ va_param_pass_value(_type_, _name_, _return_type_);va_function_ref(_type_, _return_type_, _name_)
#define va_function_ex(_type_, _return_type_, _name_, _args_, _vars_) _return_type_ va_param_pass_value_ex(_type_, _name_, _return_type_,_args_,_vars_);va_function_ref_ex(_type_, _return_type_, _name_,_args_)


