#include <stddef.h>

#include "XComponentBase.h"

#include "XStateManager.h"
#include "XStateMachine.h"
#include "IStateBase.h"

namespace Utils
{
	namespace State
	{
		XMachine::XMachine()
		{
			states = NULL;
		}

		XMachine::~XMachine()
		{
			states = NULL;
		}
		
		void XMachine::Update(Component::XBase *entity)
		{
			states = entity->Get < XSet >();
		}

		State::IBase *XMachine::GetState()
		{
			return (State::IBase *)m_pCurrent;
		}

		State::IBase *XMachine::GetState(unsigned int iID)
		{
			return states->Has(iID) ? XManager::GetInstance()->GetByID(iID) : NULL;
		}

		State::IBase *XMachine::GetStateAtIndex(unsigned int index)
		{
			return states->ValidateIndex(index) ? XManager::GetInstance()->GetByID((*states)[index]) : NULL;
		}

		State::IBase *XMachine::GetState(const char *szName)
		{
			State::IBase *state = XManager::GetInstance()->GetByName(szName);

			return states->Has(state->GetID()) ? state : NULL;
		}

		State::IBase *XMachine::operator[] (const char* szName)
		{
			State::IBase *state = (State::IBase*)(szName == NULL ? GetState() : GetState(szName));

			return state->IsNull() ? NULL : state;
		}

		bool XMachine::GoToState(const char* szName)
		{
			return ChangeState(GetState(szName));
		}

		bool XMachine::GoToState(unsigned int index)
		{
			return ChangeState(GetStateAtIndex(index));
		}

		bool XMachine::ChangeState(State::IBase *state)
		{
			return CMachine<XMachine>::ChangeState((TState*)((void*)state));
		}
	}
}
