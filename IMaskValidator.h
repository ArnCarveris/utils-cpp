#ifndef _UTILS_CPP_IMASK_VALIDATOR_H_
#define _UTILS_CPP_IMASK_VALIDATOR_H_

namespace Utils
{
	template <typename TMask, typename TAction, class TClass> class IMaskValidator
	{
	public:

		void Clear()
		{
			stage = 0;
		}

		TAction Validate(TMask mask)
		{
			if (stage < stages)
			{
				return Validate(mask, stage++);
			}

			return defaultAction;
		}

	protected:

		enum EStatus
		{
			Failed = 0,
			Partial = 1,
			Success = 2,
			Empty = 3
		};

		typedef TAction(TClass::*Callback)(TMask mask, EStatus status);

		size_t stage;
		size_t stages;

		TAction defaultAction;
		TMask defaultMask;

		TClass *object;

		struct XData
		{
			TMask mask;
			Callback callback;
		};

		XData *map;

		IMaskValidator(size_t stages, TAction defaultAction, TMask defaultMask, TClass *object)
		{
			this->defaultMask = defaultMask;
			this->defaultAction = defaultAction;

			this->stages = stages;
			this->map = new XData[stages];
			this->stage = 0;
			this->object = object;
		}

		~IMaskValidator()
		{
			delete[] map;
		}

		TAction Validate(TMask mask, size_t stage)
		{
			const XData *data = &map[stage];
			const TMask and = mask & data->mask;
			const EStatus status = (EStatus)((and != defaultMask) + (and == data->mask));

			return (object->*(data->callback))(and, data->mask == defaultMask ? EStatus::Empty : status);
		}

		bool RegisterStage(Callback callback, TMask mask)
		{
			const bool bSuccess = stage < stages;

			if (bSuccess)
			{
				map[stage].mask = mask;
				map[stage].callback = callback;
			}

			stage++;

			return bSuccess;
		}

		bool RegisterStage(Callback callback)
		{
			return RegisterStage(callback, defaultMask);
		}

		bool SetMask(TMask mask, size_t stage)
		{
			const bool bSuccess = stage < stages;

			if (bSuccess)
			{
				map[stage].mask = mask;
			}

			return bSuccess;
		}

		TMask GetMask(size_t stage)
		{
			return stage < stages ? map[stage].mask : defaultMask;
		}
	};

}

#endif