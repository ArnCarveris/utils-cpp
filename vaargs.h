
#ifndef __VA_FUNC_NAME__
#error __VA_FUNC_NAME__ not defined.
#endif

#ifndef __VA_FUNC_PARAM__
#define __VA_FUNC_PARAM__
#endif

#ifndef __VA_FUNC_ARGS__
#define __VA_FUNC_ARGS__
#endif

#ifdef __VA_ARGS__
	__VA_FUNC_NAME__(__VA_FUNC_PARAM__ __VA_FUNC_ARGS__);
#else
#define __VA_ARGS__ __VA_FUNC_ARGS__ 
	__VA_FUNC_NAME__(__VA_FUNC_PARAM__);
#undef __VA_ARGS__
#endif

#undef __VA_FUNC_ARGS__
#undef __VA_FUNC_PARAM__
#undef __VA_FUNC_NAME__