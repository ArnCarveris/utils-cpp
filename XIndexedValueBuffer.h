#pragma once

#include <vector>
#include "XValueBuffer.h"

namespace Utils
{
	class XIndexedValueBuffer
	{
	public:
		XIndexedValueBuffer(const size_t &kMaxMemory)
			:	m_pBuffer(kMaxMemory)
		{
		}
		~XIndexedValueBuffer()
		{
			m_vecOffset.clear();
		}
	
		size_t GetSize()
		{
			return m_pBuffer.GetSize();
		}

		size_t GetOffset()
		{
			return m_pBuffer.GetOffset();
		}

		size_t GetIndexCount()
		{
			return m_vecOffset.size();
		}

		XT(Value) TValue* Set(const TValue hValue, size_t *pIndex = NULL) // Creating new value with data, outputs index
		{
			size_t hIndex;

			if (pIndex == NULL)
			{
				pIndex = &hIndex;
			}

			TValue *pValue = m_pBuffer.Set <TValue> (hValue, pIndex);

			AddIndex(pIndex);

			return pValue;
		}

		XT(Value) TValue* Set(size_t *pIndex = NULL) // Creating new value, outputs index
		{
			size_t hIndex;

			if (pIndex == NULL)
			{
				pIndex = &hIndex;
			}

			TValue* pValue = m_pBuffer.Set < TValue > (pIndex);
	        
			AddIndex(pIndex);

			return pValue;
		}

		XT(Value) TValue* Set(const TValue hValue, size_t iIndex) // Setting existing value at index
		{
			return ValidIndex(iIndex) ? m_pBuffer.Set < TValue > (hValue, m_vecOffset[iIndex]) : NULL;
		}
		
		XT(Value) TValue* Get(size_t iIndex) // Getting value at index
		{
			return ValidIndex(iIndex) ? m_pBuffer.Get < TValue > (m_vecOffset[iIndex]) : NULL;
		}

		void* Get(size_t iIndex)
		{
			return ValidIndex(iIndex) ? m_pBuffer.Get (m_vecOffset[iIndex]) : NULL;
		}
	
	protected:
		
		bool ValidIndex(size_t iIndex)
		{
			return iIndex >= 0 && iIndex < GetIndexCount();
		}


		void AddIndex(size_t *pOffset)
		{
			if (pOffset != NULL)
			{
				m_vecOffset.push_back(*pOffset);

				(*pOffset) = m_vecOffset.size() - 1;
			}
		}

		XValueBuffer		m_pBuffer;
		std::vector<int>	m_vecOffset;
	};
}