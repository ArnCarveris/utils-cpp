#ifndef _UTILS_CPP_STATE_XMANAGER_H_
#define _UTILS_CPP_STATE_XMANAGER_H_

#include "XManager.h"
#include "IStateBase.h"

namespace Utils
{
	namespace State
	{
		class XManager : public Utils::XManager<IBase*, unsigned int>
		{
		public:
			static XManager * GetInstance(bool bClear = false);

		protected:

			IBase *GetByItemSet(unsigned int iID)
			{
				return iID > 0 ? *items.GetAtIndex(iID - 1) : NULL;
			}
		};
	}
}

#endif