#ifndef _UTILS_CPP_STATE_XSET_H_
#define _UTILS_CPP_STATE_XSET_H_

#include "XSet.h"
#include "XComponentNull.h"

namespace Utils
{
	namespace State
	{
		class XSet : public XIDSet, public Component::XNull { };
	}
}

#endif