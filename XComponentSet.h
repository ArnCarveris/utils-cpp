#ifndef _UTILS_CPP_COLLECTIONS_XCOMPONENT_SET_H_
#define _UTILS_CPP_COLLECTIONS_XCOMPONENT_SET_H_

#include <typeinfo>
#include <vector>
#include "XDictionary.h"
#include "XDelegate.h"
#include "Base.h"
/*
Global macros
*/

#define UtilsComponentTMessageDelegate(_component_, _message_) Utils::Delegate::X2<void, _component_, _message_>
#define UtilsComponentTSenderMessageDelegate(_component_, _sender_, _message_) Utils::Delegate::X3<void, _component_, _sender_, _message_>

#define UtilsImplementComponentTMessageCallback(_name_, _component_, _message_, _block_) void _name_ (_component_ component, _message_ message) _block_
#define UtilsImplementComponentTSenderMessageCallback(_name_, _component_, _sender_, _message_, _block_) void _name_ (_component_ component, _sender_ sender, _message_ message) _block_

#define UtilsDefineComponentTMessageCallback(_name_, _component_, _message_, _block_) typedef UtilsComponentTMessageDelegate(_component_, _message_) _name_##Delegate; UtilsImplementComponentTMessageCallback(_name_, _component_, _message_, _block_)
#define UtilsDefineComponentTSenderMessageCallback(_name_, _component_, _sender, _message_, _block_) typedef UtilsComponentTSenderMessageDelegate(_component_, _sender, _message_) _name_##Delegate; UtilsImplementComponentTSenderMessageCallback(_name_, _component_, _sender, _message_, _block_)

/*
Local macros
*/

#define TMessageDelegate(_type_) UtilsComponentTMessageDelegate(T##_type_, TMessage)
#define TSenderMessageDelegate(_type_) UtilsComponentTSenderMessageDelegate(T##_type_, TSender, TMessage)

#define C(_component_) (TBase)(_component_)

#define Method(_name_) _name_ (GetKey<TComponent>()
#define TMethod(_name_) (TComponent *) Method(_name_)

namespace Utils
{
	namespace Component
	{
		/*
		Note:
		0 - ID: means last ID
		*/
		XT(Base) class XSet // Generic component container/set class with specifed base type
		{
		public:

			typedef std::vector<TBase> TBases;

			// De/Constr //			

			~XSet()
			{
				Remove();
			}

			// Component management methods //

			XT(Component) TComponent * Create(unsigned int iID = 0)
			{
				return TMethod(Add), C(new TComponent()), iID, true);
			}

			XT(Component) TComponent * Add(TComponent * component, unsigned int iID = 0)
			{
				return TMethod(Add), C(component), iID, false);
			}

			XT(Component) void Set(TComponent * component, unsigned int iID = 0)
			{
				Method(Set), C(component), iID);
			}

			XT(Component) TComponent* Get(unsigned int iID = 0)
			{
				return TMethod(Get), iID);
			}

			XT(Component) TBases *GetAll()
			{
				return Method(GetBases), false);
			}

			XT(Component) size_t GetSize() // Get components in specified type
			{
				return Method(GetBases), false)->size();
			}

			XT(Component) bool Has(unsigned int iID)
			{
				return Method(Has), iID);
			}

			XT(Component) bool Remove(unsigned int iID = 0)
			{
				return Method(Remove), iID);
			}

			XT(Component) bool Has()
			{
				return components.Has(GetKey<TComponent>());
			}

			// Anonymous message sending methods //

			XTN(Component), typename TMessage > void Send(TMessage message, TMessageDelegate(Component*) *callback) // Send to each component childs
			{
				TBases *bases = Method(GetBases), false);

				if (bases != 0)
				{
					for (size_t i = 0, c = bases->size(); i < c; i++)
					{
						(*callback)((TComponent*)bases->at(i), message);
					}
				}
			}

			XTN(Component), typename TMessage > void Send(TMessage message, TMessageDelegate(Component*) *callback, unsigned int iID) // Send to component child with id
			{
				TBases *bases = Method(GetBases), false);

				if (bases == 0)
				{
					return;
				}

				const size_t size = bases->size();

				if (GetIndex(&size, &iID) < size)
				{
					(*callback)((TComponent*)bases->at(iID), message);
				}
			}

			XT(Message) void Send(TMessage message, TMessageDelegate(Base) *callback) // Send to all components
			{
				TBases *bases;

				for (int i = 0, c = (int)components.GetCount(); i < c; i++)
				{
					bases = components.GetAtIndex(i);

					for (size_t j = 0, l = bases->size(); j < l; j++)
					{
						(*callback)(bases->at(j), message);
					}
				}
			}

			// Sender message sending methods //

			XTN(Component), typename TSender, typename TMessage > void Send(TSender sender, TMessage message, TSenderMessageDelegate(Component*) *callback) // Send to each component childs with sender info
			{
				TBases *bases = Method(GetBases), false);

				if (bases != 0)
				{
					for (size_t i = 0, c = bases->size(); i < c; i++)
					{
						(*callback)((TComponent*)bases->at(i), sender, message);
					}
				}
			}

			XTN(Component), typename TSender, typename TMessage > void Send(TSender sender, TMessage message, TSenderMessageDelegate(Component*) *callback, unsigned int iID) //Send to component child with id & sender info
			{
				TBases *bases = Method(GetBases), false);

				if (bases == 0)
				{
					return;
				}

				const size_t size = bases->size();

				if (GetIndex(&size, &iID) < size)
				{
					(*callback)((TComponent*)bases->at(iID), sender, message);
				}
			}

			XTN(Sender), typename TMessage > void Send(TSender sender, TMessage message, TSenderMessageDelegate(Base) *callback) // Send to all components with sender info
			{
				TBases *bases;

				for (int i = 0, c = (int)components.GetCount(); i < c; i++)
				{
					bases = components.GetAtIndex(i);

					for (size_t j = 0, l = bases->size(); j < l; j++)
					{
						(*callback)(bases->at(j), sender, message);
					}
				}
			}

			// General methods //

			TBases GetAll() // Gets all components
			{
				TBases components;
				TBases *bases;

				for (int i = 0, c = (int)this->components.GetCount(); i < c; i++)
				{
					bases = this->components.GetAtIndex(i);

					components.insert(components.end(), bases->begin(), bases->end());
				}

				return components;
			}

			size_t GetSize() // Gets all components count
			{
				size_t size = 0;

				for (int i = 0, c = (int)this->components.GetCount(); i < c; i++)
				{
					size += this->components.GetAtIndex(i)->size();
				}

				return size;
			}

			size_t GetCount() // Get components type count
			{
				return components.GetCount();
			}

			bool Has()
			{
				return components.GetCount() > 0;
			}

			bool Remove()
			{
				const bool bContains = Has();

				for (int i = 0, l = (int)components.GetCount(); i < l; i++)
				{
					RemoveCreated(components.GetKeyAtIndex(i), -1);
				}

				components.Clear(); 
				created.Clear();

				return bContains;
			}

		protected:
			
			typedef std::vector<unsigned int> XIDSet;

			XT(Component) const char *GetKey()
			{
				return typeid(TComponent*).name();
			}

			unsigned int GetIndex(const size_t *size, unsigned int *iID)
			{
				if ((*iID) == 0)
				{
					(*iID) = (unsigned int)(*size);
				}

				return --(*iID);
			}

			// Internal component base management methods //
			
			TBases * GetBases(const char *key, bool bAddIfNeeded = true)
			{
				if (components.Has(key) == false)
				{
					if (bAddIfNeeded)
					{
						TBases bases;

						components.Add(key, bases);
					}
					else
					{
						return (TBases *)0;
					}
				}

				return (TBases *)components.Get(key);
			}

			TBase Add(const char *key, TBase component, unsigned int iID, bool bIsCreated)
			{
				TBases *bases = GetBases(key);

				if (iID == 0 || iID > bases->size())
				{
					bases->push_back(component);

					iID = (unsigned int)bases->size() - 1;
				}
				else
				{
					bases->insert(bases->begin() + (--iID), component);
				}

				if (bIsCreated)
				{
					if (created.Has(key) == false)
					{
						XIDSet ids;

						created.Add(key, ids);
					}

					created.Get(key)->push_back(iID);
				}

				return component;
			}

			void Set(const char *key, TBase component, unsigned int iID)
			{
				TBases *bases = GetBases(key);

				const size_t size = bases->size();

				if (size > 0)
				{
					(*bases)[GetIndex(&size, &iID)] = component;
				}
			}

			
			TBase Get(const char *key, unsigned int iID)
			{
				TBases *bases = GetBases(key);
				const size_t size = bases->size();

				if (size < 1)
				{
					return (TBase)0;
				}

				return bases->at(GetIndex(&size, &iID));
			}

			bool Has(const char *key, unsigned int iID)
			{
				TBases *bases = GetBases(key, false);

				if (bases == 0)
				{
					return false;
				}

				const size_t size = bases->size();

				return GetIndex(&size, &iID) < size;
			}
			
			bool Remove(const char *key, unsigned int iID)
			{
				if (iID-- == 0)
				{
					return Remove(key);
				}

				TBases *bases = GetBases(key, false);

				const bool bHas = bases != 0;

				if (bHas && RemoveCreated(created.Get(key), bases, iID) == false)
				{
					bases->erase(bases->begin() + iID);
				}

				return bHas;
			}

			bool Remove(const char *key)
			{
				RemoveCreated(key, -1);

				return components.Remove(key);
			}

			bool RemoveCreated(const char *key, int index)
			{
				XIDSet *ids = created.Get(key);

				if (ids == NULL)
				{
					return false;
				}

				TBases *bases = components.Get(key);

				RemoveCreated(ids, bases, index);

				if (ids->size() < 1)
				{
					created.Remove(key);
				}

				return true;
			}

			bool RemoveCreated(XIDSet *ids, TBases *bases, int index)
			{
				if (ids == NULL || bases == NULL || (size_t)index >= ids->size())
				{
					return false;
				}

				bool bAll = index < 0;

				do
				{
					unsigned int iID;
					TBase base;

					if (bAll)
					{
						iID = ids->back();
						ids->pop_back();
					}
					else
					{
						iID = ids->at(index);
						ids->erase(ids->begin() + index);
					}
					
					if (iID < bases->size())
					{
						base = bases->at(iID);
						bases->erase(bases->begin() + iID);
					}
					else
					{
						base = bases->back();
						bases->pop_back();
					}
					
					
					delete base;

					base = NULL;

				} while (bAll && ids->size() > 0);

				return true;
			}
			
			XDictionary < const char *, XIDSet>		created;
			XDictionary < const char *, TBases>		components;
		};

		typedef XSet<void *> XContainer; // Universal container for any component base kind
	}
}

#undef C
#undef Method
#undef TMethod
#undef TMessageDelegate
#undef TSenderMessageDelegate

#endif