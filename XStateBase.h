
#pragma once

namespace Utils
{
	namespace State
	{
		template <typename TMachine> class XBase
		{
		public:

			typedef XBase<TMachine> TBase;

			virtual bool OnRegister() = 0;
			virtual bool OnUnRegister() = 0;

			virtual bool OnEnter() = 0;
			virtual bool OnExit() = 0;

			virtual void OnUpdate(TMachine *pMachine, float fDeltaTime) = 0;
			
			class XNull: public TBase
			{
			public:
				bool OnRegister(){ return true; }
				bool OnUnRegister(){ return true; }

				bool OnEnter(){ return true; }
				bool OnExit(){ return true; }

				void OnUpdate(TMachine *machine, float fDeltaTime) { }

				static TBase *Self()
				{
					static XNull s_aNull;

					return &s_aNull;
				}
			};

			bool IsNull()
			{
				return this == XNull::Self();
			}

			static TBase *Validate(TBase *pState)
			{
				return pState == NULL ? XNull::Self() : pState;
			}

			TBase *Valid()
			{
				return this? this : XNull::Self();
			}

			TBase *Validated()
			{
				if (!this)
				{
					TBase ** pSelf = &((TBase *)this);

					*pSelf = XNull::Self();
				}

				return this;
			}
		};
	}
}