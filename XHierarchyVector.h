#pragma once

#include <algorithm>
#include <vector>

namespace Utils
{
	namespace Hierarchy
	{

		template <typename T, typename P, typename C = void> class CXVector
		{
		public:
			struct Node;
			//typedef std::vector<typename> Vector;
			typedef CXVector TXVector;

			typedef  std::vector<size_t> IndexPath;
			typedef  std::vector<Node> NodeVector;
			typedef  std::vector<Node*> NodePath;

			struct Node
			{
				T m_aT;
				NodeVector *m_pVector;


				Node() :m_pVector(0) {}
				Node(Node &aNode):m_pVector(aNode.m_pVector), m_aT(aNode.m_aT){}
				Node(Node *pNode):m_pVector(pNode->m_pVector), m_aT(pNode->m_aT){}
				Node(const Node &aNode):m_pVector(aNode.m_pVector), m_aT(aNode.m_aT) {}
				Node(const Node *pNode):m_pVector(pNode->m_pVector), m_aT(pNode->m_aT) {}
				Node(const T &aT) :m_pVector(0), m_aT(aT) {}

				~Node()
				{
					if (!m_pVector || CopyMode() == true) return;

					delete m_pVector;
					m_pVector = NULL;
				}

				NodeVector *CreateVector()
				{
					return (m_pVector = new NodeVector());
				}

				static bool &CopyMode()
				{
					static bool s_bMode = false;

					return s_bMode;
				}

				int Find(const T &aT, C* pContext = NULL)
				{
					return m_pVector ? CXVector::Find(*m_pVector, aT, pContext) : -1;
				}

				Node *Add(const T &aT)
				{
					return &m_pVector->at(AddWith(aT));
				}

				size_t AddWith(const T &aT)
				{
					if (!m_pVector) CreateVector();

					{
						bool& bCopyMode = CopyMode();

						bCopyMode = true;
						m_pVector->push_back(Node(aT));
						bCopyMode = false;
					}
					
					return m_pVector->size() - 1;
				}

				bool Remove(const T &aT, bool bDeep = true)
				{
					if (!m_pVector) return false;

					int nIndex = Find(aT);

					if (nIndex < 0) return false;

					if (bDeep)
					{
						m_pVector->at(nIndex).Remove(bDeep);
					}
					
					m_pVector->erase(m_pVector->begin() + nIndex);

					return true;
				}

				bool Remove(bool bDeep)
				{
					if (!m_pVector) return false;

					while(m_pVector->size() > 0)
					{
						m_pVector->back().Remove(bDeep);

						m_pVector->pop_back();
					}
					
					delete m_pVector;

					m_pVector = NULL;

					return true;
				}

				Node* Select(const IndexPath &aPath)
				{
					Node* pNode = this;

					for(IndexPath::const_iterator it = aPath.begin(); it != aPath.end(); it++)
					{
						if (pNode->m_pVector && *it >= pNode->m_pVector->size())
						{
							pNode = &pNode->m_pVector->at(*it);
						}
						else
						{
							return NULL;
						}
					}

					return pNode;
				}

				void Process(void(*pThunk)(Node*,unsigned))
				{
					Process(pThunk, this);
				}

				static void Process(void(*pThunk)(Node*,unsigned), Node *pRootNode)
				{
					if (!pRootNode->m_pVector)
					{
						return;
					}

					Node *pNode;

					for(unsigned i = 0, c = pRootNode->m_pVector->size(); i < c; i++)
					{
						pNode = &pRootNode->m_pVector->at(i); 
						
						pThunk(pNode, i);

						Process(pThunk, pNode);
					}
				}
			};

			CXVector() : m_pCurrentNode(NULL){ }
			~CXVector()
			{
				m_pCurrentNode = NULL;
				m_aCurrentIndexPath.clear();
				m_aCurrentNodePath.clear();
			}

			Node &GetRoot()
			{
				return m_aRootNode;
			}

			Node *GetCurrentNode()
			{
				return m_pCurrentNode;
			}
			
			const IndexPath &GetCurrentIndexPath()
			{
				return m_aCurrentIndexPath;
			}

			const NodePath &GetCurrentNodePath()
			{
				return m_aCurrentNodePath;
			}

			bool Has(const IndexPath &aIndexPath, Node **pNode = NULL)
			{
				if (!aIndexPath.size()) return false;

				(*pNode) = &m_aRootNode;

				for(IndexPath::iterator it = aIndexPath.begin(); it != aIndexPath.end(); it++)
				{
					if (*it < (*pNode)->m_pVector.size())
					{
						(*pNode) = &(*pNode)->m_pVector[*it];
					}
					else return false;
				}

				return true;
			}


			bool Advance(const T &aT, C *pContext = NULL)
			{	
				int nIndex = GetAdvanceIndex(aT, pContext);
				
				if (nIndex < -1)
				{
					return true;
				}
				
				if (ResetIfNeeded(nIndex))
				{
					nIndex = m_aRootNode.Find(aT, pContext);
				}

				if (nIndex < 0)
				{
					return false;
				}

				DoAdvance(nIndex);
				
				return true;
			}



		private:

			int GetAdvanceIndex(const T &aT, C *pContext = NULL)
			{
				bool bSame = m_pCurrentNode && m_pCurrentNode->m_aT == aT;

				if (bSame)
				{
					return -2;
				}
				
				return (m_aCurrentIndexPath.size() ? *m_pCurrentNode : m_aRootNode).Find(aT, pContext);
			}

			void DoAdvance(int &nIndex)
			{
				NodeVector *pVector = (m_pCurrentNode ? *m_pCurrentNode : m_aRootNode).m_pVector;

				if (pVector)
				{
					m_pCurrentNode = &(pVector->at(nIndex));
				}
					
				m_aCurrentIndexPath.push_back(nIndex);

				m_aCurrentNodePath.push_back(m_pCurrentNode);
			}

			bool ResetIfNeeded(int &nIndex)
			{
				if (nIndex >= 0)
				{
					return false;
				}

				Reset();

				return true;
			}

			void Reset()
			{
				m_pCurrentNode = NULL;
				m_aCurrentIndexPath.resize(0);
				m_aCurrentNodePath.resize(0);
			}

			static int GetIndex(const NodeVector &aVector, typename NodeVector::const_iterator it)
			{
				return it == aVector.end() ? -1 : it - aVector.begin();
			}

			static int Find(const NodeVector &aVector, const T &aT, C *pContext = NULL)
			{
				return GetIndex(aVector, std::find_if(aVector.begin(), aVector.end(), pContext ? P(aT, pContext) : P(aT))); 
			}

			Node m_aRootNode;
			Node *m_pCurrentNode;
			IndexPath m_aCurrentIndexPath;
			NodePath m_aCurrentNodePath;
		};
	}
}
