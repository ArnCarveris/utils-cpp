/*
	(c) Sergey Ryazanov (http://home.onego.ru/~ryazanov)

	Fast delegate compatible with C++ Standard.

	Changed code style by ArnCarveris
*/
#ifndef SRUTIL_DELEGATE_INCLUDED
#define SRUTIL_DELEGATE_INCLUDED

#include "Base.h"

#include <stddef.h>
#include <setjmp.h>

// without noinline some compilers tend to allocate the array before setjmp()
#ifdef __GNUC__
#define NOINLINE __attribute__((noinline))
#else
#define NOINLINE __declspec(noinline)
#endif


#ifdef Yield
#undef Yield
#endif

namespace Utils
{
	namespace Delegate
	{
		namespace Coroutine
		{
			class IBase
			{

			public:
				IBase()
				{
					m_bInitialized = false;
				}

				void Yield()
				{
					if (setjmp(m_iPoint[1]) == 0)
					{
						longjmp(m_iPoint[0], 3);
					}
				}

			protected:

				jmp_buf			m_iPoint[2];

				bool			m_bInitialized;
			};

			XT(Return) class XBase : public IBase
			{
			public:
				XBase() : IBase()
				{
					m_xValue = 0;
				}

				void Yield()
				{
					IBase::Yield();
				}

				void Yield(TReturn value)
				{
					m_xValue = value;

					Yield();
				}

			protected:

				TReturn			m_xValue;
			};
		}
	}
}

#ifdef SRUTIL_DELEGATE_PREFERRED_SYNTAX
namespace Utils
{
	namespace Delegate
	{
		XT(Signature) class X;

		namespace Invoker
		{
			XT(Signature) class X;
		}

		namespace Coroutine
		{
			XT(Signature) class X;
		}
	}
}
#endif

#ifdef _MSC_VER
#define SRUTIL_DELEGATE_CALLTYPE __fastcall
#else
#define SRUTIL_DELEGATE_CALLTYPE
#endif

#include "delegate_detail/list.h"

#undef SRUTIL_DELEGATE_CALLTYPE

#define typedef_utils_delegate_x(_type_, _name_) typedef Utils::Delegate::X##_type_ _name_

#endif//SRUTIL_DELEGATE_INCLUDED
