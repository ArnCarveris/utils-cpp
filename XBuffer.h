#pragma once

namespace Utils
{
	XT(Block) class XBuffer
	{
	public:

		XBuffer() : m_pBuffer(0), m_iOffset(0), m_iSize(0) { }
		XBuffer(const size_t kMaxMemory)
		{
			Init(kMaxMemory);
		}
		XBuffer(const XBuffer &aBuffer)  : m_pBuffer(aBuffer.m_pBuffer), m_iOffset(aBuffer.m_iOffset), m_iSize(aBuffer.m_iSize) { }
		

		XBuffer& Init(const size_t &kMaxMemory)
		{
			m_iOffset = 0;
			m_iSize = kMaxMemory;
			
			if (kMaxMemory > 0)
			{
				m_pBuffer = new TBlock[kMaxMemory];
			}
			else
			{
				m_pBuffer = NULL;
			}
			
			return *this;
		}

		XBuffer& Release()
		{
			if (m_pBuffer)
			{
				delete [] m_pBuffer;
				m_iOffset = 0;
				m_iSize = 0;
			}

			return *this;
		}

		virtual ~XBuffer()
		{

		}

		size_t GetOffset()
		{
			return m_iOffset;
		}

		size_t GetSize()
		{
			return m_iSize;
		}

		void* Get(size_t iOffset)
		{
			if (m_iOffset < iOffset)
			{
				return NULL;
			}

			return ((void*)((uintptr_t)m_pBuffer + iOffset));
		}

	protected:
		TBlock*				m_pBuffer;
		size_t				m_iOffset;
		size_t				m_iSize;
	};

}