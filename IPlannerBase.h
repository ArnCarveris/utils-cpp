#ifndef _UTILS_CPP_PLANNER_IBASE_H_
#define _UTILS_CPP_PLANNER_IBASE_H_

#include <vector>

#include "XComponentNull.h"

namespace Utils
{
	namespace Planner
	{
		//Note:
		// var's named 'node' with type 'unsigned int' are ID that are passed to data provider


		struct IData // Planner custom data provider inteface
		{
			int count;

			std::vector<int> costs;
			std::vector<unsigned int> nodes;

			virtual void PerpareGathering() = 0;

			virtual void PrintData() = 0;

			virtual bool Match(unsigned int goal) = 0;

			virtual void GatherData(unsigned int goal) = 0;

			virtual int CalculateH(unsigned int node, unsigned int to) = 0;

			virtual bool Equals(unsigned int node, unsigned int value) = 0;

			virtual void UpdateCurrentNode(unsigned int node, unsigned int supernode) = 0;

			int CalculateH(int index, unsigned int to)
			{
				return CalculateH(nodes[index], to);
			}
		};


		struct IResult // Planners result class interface
		{
			int cost;
			int count;
			std::vector<unsigned int> nodes;
		};

		struct IBase : public Component::XNull// Planners Main Interface
		{
			virtual void Search(IData *data, unsigned int current, unsigned int goal, IResult *result) = 0;
		};
	}
}

#endif