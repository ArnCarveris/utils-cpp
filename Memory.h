#ifndef UTILS_CPP_MEMORY_H
#define UTILS_CPP_MEMORY_H

#include "Base.h"

#include <windows.h>

namespace Utils
{
	namespace Memory
	{
		typedef size_t TBlock;

		template<class T> struct Class
		{
			enum EType
			{
				Type_Value = 0,
				Type_Method,
				Type_Pointer,
				Type_Reference,
				Type_Array
			};

			template<typename M> struct IsMethod
			{
				static const bool Value = true;
			};

			template<typename M> struct IsMethod<M T::*>
			{
				static const bool Value = false;
			};

			template<typename M> static bool IsMethodQ(M m)
			{
				return IsMethod<M>::Value;
			}

			template<typename M> struct Member
			{


				struct Value
				{
					typedef M T::* Type;
				};

				struct Method
				{
					typedef M (T::*Type)(...);
				};
				
				static TBlock Offset(const Value::Type pMember)
				{
					return (TBlock) &( reinterpret_cast<T*>(0)->*pMember);
				}
				static T* Container(M* ptr, const Value::Type pMember)
				{
					return (T*)( (char*)ptr - Offset(pMember));
				}
			};

			template<typename M> static TBlock OffsetOf(const M T::*pMember)
			{
				return Member::Offset<M>(pMember);
			}
			template<typename M> static T* ContainerOf(M* pM, const M T::*pMember)
			{
				return Member::Container<M>(pM, pMember);
			}
			
		};

		template <TBlock NX> static inline bool DiveBegin(void *p, TBlock*& pBlock)
		{
			if (NX == 0 || !p)
			{
				pBlock = 0;	

				return false;
			}
			
			pBlock = (size_t*)p + NX;

			return true;
		}

		template <TBlock NX> static inline bool DiveNext(TBlock*& pBlock)
		{
			if (NX == 0 || !pBlock)
			{
				return false;
			}

			pBlock = (size_t*)(*pBlock) + NX;

			return true;
		}
		
		namespace Accessor
		{


			template <
				TBlock N1, 
				TBlock N2 = 0, 
				TBlock N3 = 0, 
				TBlock N4 = 0, 
				TBlock N5 = 0, 
				TBlock N6 = 0, 
				TBlock N7 = 0, 
				TBlock N8 = 0, 
				TBlock N9 = 0, 
				TBlock N10 = 0
			> struct TAddress
			{
				static inline TBlock* Get(void *p)
				{
					TBlock* pBlock;
					(
						DiveBegin<N1>(p, pBlock)
						&& DiveNext<N2>(pBlock)
						&& DiveNext<N3>(pBlock)
						&& DiveNext<N4>(pBlock)
						&& DiveNext<N5>(pBlock)
						&& DiveNext<N7>(pBlock)
						&& DiveNext<N8>(pBlock)
						&& DiveNext<N9>(pBlock)
						&& DiveNext<N10>(pBlock)
					);
					return pBlock;
				}
			};
			
			template <typename T, typename TAddressBlock> struct SVariable
			{
				SVariable(void *pT)
				{
					m_pT = (T*)TAddressBlock::Get(p);
					m_aT = *m_pT;
				}

				inline void operator = (const T &aT)
				{
					*m_pT = aT;
				}

				~SVariable()
				{
					*m_pT = m_aT;
				}

				T m_aT;
				T* m_pT;
			};

			template <typename T, typename TAddressBlock> struct TValue
			{
				T *m_pT;

				TValue()
				{
					Unset();
				}
				TValue(void *p)
				{
					Set(p);
				}
				~TValue()
				{
					Unset();
				}
				
				template <typename TOtherAddressBlock> inline bool Set(void* p, const TValue<void*, TOtherAddressBlock>& krValue)
				{
					return krValue.Set(p) && Set(*krValue);
				}

				inline bool Set(void *p)
				{
					const bool kbIsNonNull = p != NULL;

					if (kbIsNonNull)
					{
						m_pT = (T*)TAddressBlock::Get(p);
					}
					else
					{
						Unset();
					}

					return kbIsNonNull;
				}

				inline void Unset()
				{
					m_pT = NULL;
				}

				inline bool IsSet() const
				{
					return m_pT != 0;
				}

				inline T*& Get() 
				{
					return m_pT;
				}

				inline T& operator = (const T& aT)
				{
					return (*m_pT = aT);
				}

				inline T& operator * ()
				{
					return *m_pT;
				}

				inline T& operator -> ()
				{
					return *m_pT;
				}
			};

			template <typename T, typename TAddressBlock> struct TArray : TValue<T*, TAddressBlock>
			{
				inline T& operator[] (TBlock i)
				{
					return (operator* ())[i];
				}
			};

			template <typename T, 
				TBlock N1,
				TBlock N2 = 0, 
				TBlock N3 = 0, 
				TBlock N4 = 0, 
				TBlock N5 = 0, 
				TBlock N6 = 0, 
				TBlock N7 = 0, 
				TBlock N8 = 0, 
				TBlock N9 = 0, 
				TBlock N10 = 0
			> struct TValueAt : TValue<T, TAddress<N1, N2, N3, N4, N5, N6, N7, N8, N9, N10> >
			{
				typedef TValue<T, TAddress<N1, N2, N3, N4, N5, N6, N7, N8, N9, N10> > SuperClass;

				inline T& operator = (const T& aT)
				{
					return SuperClass::operator = (aT);
				}

				inline T& operator * ()
				{
					return SuperClass::operator * ();
				}

				inline T& operator -> ()
				{
					return SuperClass::operator -> ();
				}
			};

			template <typename T, 
				TBlock N1,
				TBlock N2 = 0, 
				TBlock N3 = 0, 
				TBlock N4 = 0, 
				TBlock N5 = 0, 
				TBlock N6 = 0, 
				TBlock N7 = 0, 
				TBlock N8 = 0, 
				TBlock N9 = 0, 
				TBlock N10 = 0
			> struct TArrayAt : TArray<T, TAddress<N1, N2, N3, N4, N5, N6, N7, N8, N9, N10> >
			{
				typedef TArray<T, TAddress<N1, N2, N3, N4, N5, N6, N7, N8, N9, N10> > SuperClass;

				inline T& operator[] (TBlock i)
				{
					return SuperClass::operator [] (i);
				}
			};

			namespace Virtual
			{
				struct SObject
				{
					void* m_pAddress;
					TBlock m_nSize;
					DWORD  m_nProtection;

					SObject()
					{
						Unset();
					}
					~SObject()
					{
						if (IsSet())
						{
							Protect();
						}

						Unset();
					}

					inline void Set(TBlock nAddress, TBlock nSize)
					{
						Set((void*)nAddress, nSize);
					}

					inline void Set(void *pAddress, TBlock nSize)
					{
						m_pAddress = pAddress;
						m_nSize = nSize;
					}

					inline void Unset()
					{
						m_pAddress = 0;
						m_nSize = 0;
					}

					inline bool IsSet() const
					{
						return m_pAddress && m_nSize;
					}

					inline void Unprotect()
					{
						VirtualProtect(m_pAddress, m_nSize, PAGE_EXECUTE_READWRITE, &m_nProtection ); 
					}

					inline void Protect()
					{
						VirtualProtect(m_pAddress, m_nSize, m_nProtection, 0 );
					}
					
					template <typename T> inline T& Get(TBlock nIndex)
					{
						return ((T*)m_pAddress)[nIndex];
					}
					
					template <typename T> inline const T& Get (TBlock nIndex) const
					{
						return ((const T*)m_pAddress)[nIndex];
					}
				};

				template <typename T, TBlock nAddress, TBlock N = 1> struct SRegion : SObject
				{
					SRegion()
					{
						Set(nAddress, sizeof(T) * N);
						Unprotect();
					}

					~SRegion()
					{
						Protect();
						Unset();
					}

					inline SRegion& operator = (const T& krT)
					{
						*((T*)m_pAddress) = krT;

						return *this;
					}

					inline const T& operator [] (size_t i) const
					{
						return Get(i);
					}

					inline T& operator [] (size_t i)
					{
						return Get(i);
					}
				};

				struct STable : SObject
				{
					static inline TBlock DecodeAddress(const TBlock knAddress)
					{
						return *(TBlock*)(knAddress + 1) + knAddress + sizeof(TBlock) + 1;
					}

					static inline TBlock DecodeIndexAtAddress(const TBlock knAddress)
					{
						const TBlock knSize = sizeof(TBlock);
						const TBlock knR = 0xCC | (TBlock)(-1) << (knSize * 2);
						
						TBlock nI = 0;
						TBlock nM = DecodeAddress(knAddress) + knSize;
						
						for(; nI < knSize; nI++)
						{
							if (*(char*)(nM + nI) == knR)
							{
								break;
							}
						}
						
						const TBlock knO = (knSize - nI) * (knSize * 2);
						
						nM = *(TBlock*)nM;
						
						if (knO != 0)
						{
							nM <<= knO;
							nM >>= knO;
						}

						return nM / knSize;
					}

					static inline TBlock DecodeAddressIn( TBlock **vtbl, const size_t knIndex)
					{
						return DecodeAddress((TBlock)vtbl[knIndex]);
					}

					static inline void EncodeWith(void* pDstAddress, const size_t knSrcAddress) 
					{
						*(char*)pDstAddress = (char)0xE9;

						*(TBlock*)((TBlock)pDstAddress + 1) = knSrcAddress - (TBlock)pDstAddress - (sizeof(TBlock) + 1);
					}

					template <typename T> static inline TBlock **Get(T* pT)
					{
						return *(TBlock***)pT;	
					}

					struct SItem
					{
						TBlock m_nIndex;
						TBlock m_nAddress;

						SItem() { }
						SItem(const TBlock knAddressForIndex, TBlock **vtbl) : 
							m_nIndex(DecodeIndexAtAddress(knAddressForIndex)), 
							m_nAddress(DecodeAddressIn(vtbl, m_nIndex))
						{ }

						SItem(const TBlock knAddressForIndex, const TBlock knAddress) : 
							m_nIndex(DecodeIndexAtAddress(knAddressForIndex)), 
							m_nAddress(knAddress)
						{ }

						SItem(const SItem& krOther) : 
							m_nIndex(krOther.m_nIndex),
							m_nAddress(krOther.m_nAddress)
						{ }

						inline void SetAddressIn(TBlock **vtbl)
						{
							m_nAddress = DecodeAddressIn(vtbl, m_nIndex);
						}

						inline void SetFrom(TBlock **vtbl, const size_t knIndex)
						{
							m_nIndex = knIndex;
							m_nAddress = (size_t)vtbl[knIndex];
						}

						template <typename T, typename R> static inline SItem Create(R T::*pMember, TBlock **vtbl)
						{
							return SItem(union_cast<R T::*, TBlock>(pMember).b, vtbl);
						}
						template <typename T, typename R> static inline SItem Create(R T::*pMember, const size_t knAddress = 0)
						{
							return SItem(union_cast<R T::*, TBlock>(pMember).b, knAddress);
						}

					};

					
					template <TBlock N> inline void Encode(TBlock **vtbl, SItem(&rasItem)[N])
					{
						m_nSize = sizeof(TBlock) + 1;

						const TBlock knJ = 0xE9 | (TBlock)(-1) << (sizeof(TBlock) * 2);

						for(SItem* it = &rasItem[0], *end = &rasItem[N]; it != end; it++)
						{
							m_pAddress = &vtbl[it->m_nIndex];

							if ((char)**(char***)m_pAddress == knJ)
							{
								m_pAddress = *(size_t**)m_pAddress;

								Unprotect();

								EncodeWith(m_pAddress, it->m_nAddress);
							}
							else
							{
								Unprotect();

								*(TBlock*)m_pAddress = it->m_nAddress;
							}
							
							Protect();
						}
					}

					
					template <TBlock N> static inline void PerformEncoding(TBlock **vtbl, SItem(&rasItem)[N])
					{
						STable().Encode<N>(vtbl, rasItem);
					}
				};
			}
		}
	}
}
#endif