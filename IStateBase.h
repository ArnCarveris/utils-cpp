#ifndef _UTILS_CPP_STATE_IBASE_H_
#define _UTILS_CPP_STATE_IBASE_H_

#include "XItem.h"
#include "XStateBase.h"

namespace Utils
{
	namespace State
	{
		class XMachine;

		class IBase : public XItem, public XBase<XMachine> {};
	}
}

#endif