#ifndef _UTILS_CPP_BASE_H_
#define _UTILS_CPP_BASE_H_

#pragma warning( disable : 4996 ) 
#pragma warning( disable: 4995 ) 
#define iterator_each(_type_, _container_, _name_) (_type_::iterator _name_=_container_.begin(); _name_!=_container_.end(); _name_++)
#define vector_each(_type_, _vector_, _name_) iterator_each(std::vector< _type_ >, _vector_, _name_)

#define ForBool(name, block) for (unsigned short u##name = 0; u##name < 2; u##name++){bool b##name = u##name > 0; {block}}

#define DeclareAccessor(_name_, _type_, _var_, _access_) public: void Set##_name_ ( _type_ ); _type_ Get##_name_ (); _access_##: _type_ _var_
#define ImplementAccessor(_class_, _name_, _type_, _var_, _block_) void _class_::Set##_name_ (_type_ value) {if (value != _var_) { _block_ _var_ = value; }} _type_ _class_::Get##_name_ () {return _var_;}

#ifdef XTN
#undef XTN
#endif

#ifdef XT
#undef XT
#endif

#ifdef _
#undef _
#endif

#define XTN(_type_) template <typename T##_type_
#define XT(_type_) XTN(_type_)>

#define _(_v_) , _v_
#define _XT(_type_) _(typename T##_type_)

#ifndef __VA_ARGS__
#define __VA_DEF__
#else
#define __VA_DEF__ ...
#endif
//#define OVERRIDE(_type_, _class_, _name_) using _class_ :: _name_; _type_ _name_

template <typename A, typename B> union union_cast
{
	union_cast(A aa) : a(aa) { }
 
    A a;
    B b;
};

#endif