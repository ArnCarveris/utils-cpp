#ifndef _UTILS_CPP_COMPONENT_IBASE_H_
#define _UTILS_CPP_COMPONENT_IBASE_H_

namespace Utils
{
	namespace Component
	{
		class XBase;
		class IBase
		{
		public:
			enum EDependence
			{
				None,
				Valid,
				Invalid
			};

			virtual void Initialize(XBase *entity) = 0;

			virtual void Update(XBase *entity) = 0;

			virtual void Shutdown(XBase *entity) = 0;

			EDependence GetDependence()
			{
				return eDependence;
			}

		protected:
			EDependence eDependence;
		};
	}
}

#endif