#include "XDelegateStageTester.h"
#include <stdio.h>

namespace Utils
{

	XDelegateStageTester::XDelegateStageTester()
	{
		tests = 0;
		count = 0;
		format = 0;
	}


	void XDelegateStageTester::SetUp(XTest *tests, size_t count, const char *format)
	{
		this->tests = tests;
		this->count = count;
		this->format = format;
	}

	void XDelegateStageTester::Run(bool bWait)
	{
		for (size_t i = 0; i < count; i++)
		{
			printf(format, tests[i].name);

			tests[i].callback();

			getchar();
		}

		if (bWait)
		{
			getchar();
		}
	}
}