#pragma once

#include <stdlib.h> 

namespace Utils
{
	namespace Math
	{
		struct SNumber
		{
			typedef void (SNumber::*SetAtAddressThunk)(void*);
			enum EValue
			{
				Value_Undefined = 0,
				Value_Next,
				Value_LastInSet,
				Value_Last
			};
			struct STemp
			{
				const char *m_szData;
				bool m_bIsFloat;
				
				inline void Reset()
				{
					m_szData = NULL;
					m_bIsFloat = false;
				}

			} m_aTemp;
			
			union SValue
			{
				float m_Float;
				int m_Int;
			} m_aValue;

			struct SIndex
			{
				size_t m_nValue;
				bool n_bIsLast;

				inline void Reset()
				{
					m_nValue = (size_t)-1;
					n_bIsLast = false;
				}

				inline void ResetIfNeeded()
				{
					if (n_bIsLast)
					{
						Reset();
					}
				}

				inline void Next()
				{
					m_nValue = m_nValue < (size_t)-1 ? m_nValue + 1 : 0;
				}
			};

			SIndex m_aIndex;
			SIndex m_aSet;

			bool m_bIsFloat;
			char *it;

			SNumber(const char *szText)
			{
				m_aIndex.Reset();
				m_aSet.Reset();

				m_aTemp.Reset();

				it = (char*)&szText[0];
			}

			inline operator bool() const
			{
				return it != NULL;
			};

			inline char* operator ++ (int)
			{
				return it += it != NULL;
			}

			inline EValue GetType()
			{
				size_t nType = 0;
			
				switch(*it)
				{
					case 0: it = NULL; break;
					case 'f': *it = 0;
					case '.':m_aTemp.m_bIsFloat = true;
					case '-':
					case '0':
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
					case '6':
					case '7':
					case '8':
					case '9':
						if (!m_aTemp.m_szData)
						{
							m_aTemp.m_szData = it;

							m_aIndex.ResetIfNeeded();
							m_aSet.ResetIfNeeded();

						} break;
					break;
					case ';': nType++;
					case ':': nType++;m_aIndex.n_bIsLast = true;
					case ',': nType++;m_aIndex.Next();
						{
							switch((EValue)nType)
							{
								case Value_LastInSet:	m_aSet.Next();				break;
								case Value_Last:		m_aSet.n_bIsLast = true;	break;
							}

							*it = 0;

							if ((m_bIsFloat = m_aTemp.m_bIsFloat))
							{
								m_aValue.m_Float = atof(m_aTemp.m_szData);					
							}
							else
							{
								m_aValue.m_Int = atoi(m_aTemp.m_szData);
							}

							m_aTemp.Reset();
							
						} break;
				}

				return (EValue)nType;
			}

			template <int nCoef> inline void GetFloat(float& rFloat)
			{
				if (nCoef <= 1)
				{
					GetFloat<0>(rFloat);
					
					return;
				}

				static const float fCoef = (float)nCoef;
				static const float s_kfCoef = 1.0f / fCoef;
				
				if (m_bIsFloat)
				{
					if (m_aValue.m_Float >= fCoef)
					{
						rFloat = fCoef;
					}
					else if (m_aValue.m_Float > 1)
					{
						rFloat = s_kfCoef * m_aValue.m_Float;			
					}
					else
					{
						rFloat = m_aValue.m_Float;
					}
				}
				else 
				{
					if (m_aValue.m_Int > nCoef)
					{
						rFloat = (float)m_aValue.m_Int;
					}
					else
					{
						rFloat = s_kfCoef * (float)m_aValue.m_Int;
					}
				}
			}

			template <> inline void GetFloat<0>(float& rFloat)
			{
				rFloat = m_bIsFloat ? m_aValue.m_Float : (float)m_aValue.m_Int;
			}

			inline void GetInt(int& rInt)
			{
				rInt = m_bIsFloat ? (int)m_aValue.m_Float : m_aValue.m_Int;
			}
			

			inline void SetIntAtAddress(void *pAddress)
			{
				GetInt(*(int*)pAddress);
			}

			template <int nCoef> inline void SetFloatAtAddress(void *pAddress)
			{
				GetFloat<nCoef>(*(float*)pAddress);
			}
		};
	}
}