#include "XStateManager.h"

namespace Utils
{
	namespace State
	{
		XManager * XManager::GetInstance(bool bClear)
		{
			static XManager *manager = NULL;

			if (bClear)
			{
				delete manager;

				manager = NULL;
			}
			else if (manager == NULL)
			{
				manager = new XManager();
			}

			return manager;
		}
	}
}