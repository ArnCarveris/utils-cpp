#ifndef _UTILS_CPP_XSTATE_MACHINE_H_
#define _UTILS_CPP_XSTATE_MACHINE_H_

#include "XStateManager.h"
#include "XStateSet.h"
#include "IComponentBase.h"
#include "StateMachine.h"

namespace Utils
{
	namespace State
	{

		class IBase;
	

		class XMachine : public Component::IBase, public CMachine<XMachine>
		{
		public:

			XMachine();

			virtual ~XMachine();

			virtual void Update(Component::XBase *entity);


			State::IBase *GetState();
			
			State::IBase *GetState(unsigned int iID);

			State::IBase *GetStateAtIndex(unsigned int index);

			State::IBase *GetState(const char *szName);

			State::IBase *operator[] (const char* szName);

			bool GoToState(const char* szName);

			bool GoToState(unsigned int index);


		protected:
			
			bool ChangeState(State::IBase *state);

			XSet *				states;
			State::IBase *		current;

		};
	}
}

#endif