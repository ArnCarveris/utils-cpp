#ifndef _UTILS_CPP_PLANNER_XASTAR_H_
#define _UTILS_CPP_PLANNER_XASTAR_H_

#include "IPlannerBase.h"

namespace Utils
{
	namespace Planner
	{
		class XAStar : public IBase
		{

		private:
			struct XNode
			{
				int g;				//!< The cost so far.
				int h;				//!< The heuristic for remaining cost (don't overestimate!)
				int f;				//!< g+h combined.

				unsigned int node;
				unsigned int supernode;

				void Update(int cost, int score, unsigned int new_node, unsigned int new_supernode);

				void Reset();

				void Copy(XNode *source);

				void SetCostWithScore(int cost, int score);
			};

			std::vector<XNode> nodes[2];

			int	count[2];

			bool ValidIdx(int idx, bool bClosed);

			int GetCount(bool bClosed);

			int SetCount(int new_count, bool bClosed);

			void SetNodeAtIndex(int idx, bool bClosed, XNode node);

			XNode *NodeAtIndex(int idx, bool bClosed);

			void RemoveNodeAtIndex(XNode *node, int idx, bool bClosed, int iRemoveCost = 0);

			void AddNode(XNode node, bool bClosed);

			int IdxInNode(IData *data, unsigned int node, bool bClosed);

			void Reconstruct(IData *data, IResult * result, XNode *goal);

			void AddNode(int cost, int score, unsigned int new_node, unsigned int new_supernode, bool bClosed);

			void Clear(IResult *result, int cost);

			int GetLowestNodeIndex(bool bClosed);

		public:

			void Search(IData *data, unsigned int begin, unsigned int goal, IResult *result);
		};
	}

}

#endif