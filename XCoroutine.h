#ifndef _UTILS_CPP_XCOROUTINE_H_
#define _UTILS_CPP_XCOROUTINE_H_

#include "Base.h"

#ifdef UtilsCoroutineUseSwitchBased

namespace Utils
{
	namespace Coroutine
	{
		struct XBase
		{
			unsigned int _line_;

			XBase() : _line_(0) { }
		};
	}
}

#define UtilsCoroutine(_type_, _name_, _args_, _vars_, _block_) struct _name_ : public Utils::Coroutine::XBase { _vars_ _name_() : Utils::Coroutine::XBase() _block_ _type_ operator() ( _args_ ) { switch(_line_) { case 0:  

#define UtilsCoroutineYield(_block_, _value_) { _block_ _line_ = __LINE__; return _value_; case __LINE__:; }

#define UtilsCoroutineDeclare(_type_, _name_, _args_, _vars_, _block_) struct _name_ : public Utils::Coroutine::XBase { _vars_ _name_() : Utils::Coroutine::XBase() _block_ _type_ operator() ( _args_ ); }

#define UtilsCoroutineImplement(_type_, _name_, _args_, _block_) _name_ :: _name_ () : Utils::Coroutine::XBase() _block_ _type_ _name_ :: operator() ( _args_ ) { switch(_line_) { case 0: 

#define UtilsCoroutineImplementEnd(_block_, _value_) } _block_ _line_ = 0; return _value_; }

#define UtilsCoroutineEnd(_block_, _value_)  UtilsCoroutineImplementEnd(_block_, _value_) }

#else

#include <stdio.h>
#include <stddef.h>
#include <setjmp.h>

// without noinline some compilers tend to allocate the array before setjmp()
#ifdef __GNUC__
#define NOINLINE __attribute__((noinline))
#else
#define NOINLINE __declspec(noinline)
#endif


namespace Utils
{
	namespace Coroutine
	{
		struct XBase
		{
			typedef XBase super;

			jmp_buf Point[2];

			bool initialized;

			XBase() : initialized(false) { }

			void yield()
			{
				if (setjmp(Point[1]) == 0)
				{	
					longjmp(Point[0], 3);
				}
			}

			template <typename T> NOINLINE void initalize_stack()
			{
				unsigned char stack_buffer[1 << 16];
				
				initialized = ptrdiff_t(stack_buffer) != 0;
				
				((T*)this)->call();
			}

			template <typename T> void call (T*)
			{
				if (setjmp(Point[0]) == 0)
				{
					if (initialized)
					{
						longjmp(Point[1], 3);
					}
					else
					{
						initalize_stack<T>();
					}
				}
			}
		};
	}
}

#define UtilsCoroutineVoid(_name_,_args_, _vars_, _block_) struct _name_ :  Utils::Coroutine::XBase { _vars_ void operator() (_args_) {{_block_} super::call(this);} void call(void) 
#define UtilsCoroutine(_type_, _name_, _args_, _vars_, _block_) struct _name_ : Utils::Coroutine::XBase { _vars_ _type_ value; _type_ operator() (_args_) {{_block_} super::call(this); return value;} void yield(_type_ _value_){value = _value_; super::yield();} void call(void)

#endif
#endif