#include "XItem.h"

namespace Utils
{
	ImplementAccessor(XItem, ID, unsigned int, iID, );
	ImplementAccessor(XItem, Name, const char *, szName, );
}