#ifndef _UTILS_CPP_XSET_H_
#define _UTILS_CPP_XSET_H_

#include <vector>

namespace Utils
{
	template <typename T> class XSet
	{

	protected:

		typedef std::vector<T> TItems;

		TItems items;

	public:

		void Add(T item, bool bUnique = false)
		{
			if (bUnique && Has(item))
			{
				return;
			}

			items.push_back(item);
		}

		bool Has(const T& item) const
		{
			for (size_t i = 0, l = items.size(); i < l; i++)
			{
				if (items[i] == item)
				{
					return true;
				}
			}
			return false;
		}

		bool Set(T item, int index)
		{
			const bool bSuccess = ValidateIndex(index);

			if (bSuccess)
			{
				items[index] = item;
			}

			return bSuccess;
		}

		T& Get(int index)
		{
			return ValidateIndex(index) ? items.at(index) : (T)0;
		}

		bool ValidateIndex(int index) // TODO: change method name
		{
			return index >= 0 && index < (int)GetSize();
		}

		void Remove(T item)
		{
			items.erase(std::remove(items.begin(), items.end(), item), items.end());
		}

		void RemoveAtIndex(int index)
		{
			items.erase(items.begin() + index);
		}

		T& operator[] (int index)
		{
			return items.at(index);
		}

		size_t GetSize()
		{
			return items.size();
		}

		void Clear()
		{
			items.clear();
		}

		void Copy(XSet *set)
		{
			Clear();

			items.insert(items.begin(), set->items.begin(), set->items.end());
		}

		void Merge(XSet *set, int pos)
		{
			const int size[2] = {
				items.size(),
				set->items.size()
			};

			if (pos < 0 || pos >= size[0])
			{
				for (int i = 0; i < size[1]; i++)
				{
					items.push_back(set->items[i]);
				}
			}
			else
			{
				items.insert(items.begin() + pos, set->items.begin(), set->items.end());
			}
		}

		int IndexOf(T item)
		{
			if (item != (T)0)
			{
				for (int i = 0, c = items.size(); i < c; i++)
				{
					if (item == items[i])
					{
						return i;
					}
				}
			}

			return -1;
		}
	};

	typedef XSet<unsigned int> XIDSet;
	typedef XSet<const char*> XNameSet;
}

#endif