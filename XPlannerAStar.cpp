#include <limits.h>
#include <algorithm>

#include "XPlannerAStar.h"
#include "Base.h"

namespace Utils
{
	namespace Planner
	{
		void XAStar::XNode::Update(int cost, int score, unsigned int new_node, unsigned int new_supernode)
		{
			node = new_node;
			supernode = new_supernode;

			SetCostWithScore(cost, score);
		}


		void XAStar::XNode::SetCostWithScore(int cost, int score)
		{
			g = cost;
			h = score;
			f = g + h;
		}

		void XAStar::XNode::Reset()
		{
			f = g = h = 0;

			node = NULL;
			supernode = NULL;
		}

		void XAStar::XNode::Copy(XNode *source)
		{
			if (source == NULL)
			{
				return;
			}

			f = source->f;
			g = source->g;
			h = source->h;

			node = source->node;
			supernode = source->supernode;
		}

		bool XAStar::ValidIdx(int idx, bool bClosed)
		{
			return idx >= 0 && idx < count[bClosed];
		}

		int XAStar::GetCount(bool bClosed)
		{
			return count[bClosed];
		}

		int XAStar::SetCount(int new_count, bool bClosed)
		{
			return (count[bClosed] = new_count);
		}

		void XAStar::SetNodeAtIndex(int idx, bool bClosed, XNode node)
		{
			if (ValidIdx(idx, bClosed))
			{
				nodes[bClosed][idx] = node;
			}
		}

		XAStar::XNode* XAStar::NodeAtIndex(int idx, bool bClosed)
		{
			return ValidIdx(idx, bClosed) ? &nodes[bClosed][idx] : NULL;
		}

		void XAStar::RemoveNodeAtIndex(XNode *node, int idx, bool bClosed, int iRemoveCost)
		{
			if (node != NULL)
			{
				node->Copy(&nodes[bClosed][idx]);
			}

			if (count[bClosed] && (iRemoveCost < 1 || (node != NULL && iRemoveCost < node->g)))
			{
				nodes[bClosed][idx].Copy(&nodes[bClosed][--count[bClosed]]);
				nodes[bClosed].pop_back();
			}
		}

		void XAStar::AddNode(XNode node, bool bClosed)
		{
			nodes[bClosed].push_back(node);
			count[bClosed]++;
		}

		int XAStar::IdxInNode(IData *data, unsigned int node, bool bClosed)
		{
			for (int i = 0; i < count[bClosed]; i++)
			{
				if (data->Equals(nodes[bClosed][i].node, node))
				{
					return i;
				}
			}

			return -1;
		}

		void XAStar::Reconstruct(IData *data, IResult * result, XNode *goal)
		{
			Clear(result, 0);

			XNode *current = new XNode();

			current->Copy(goal);

			bool bHasNode = true;

			while (current->node > 0 && bHasNode)
			{
				bHasNode = current->supernode > 0;

				result->count++;

				result->cost += current->f;

				result->nodes.push_back(current->node);

				current->Copy(NodeAtIndex(IdxInNode(data, current->supernode, true), true));
			}

			delete current;

			std::reverse(result->nodes.begin(), result->nodes.end());
		}

		void XAStar::AddNode(int cost, int score, unsigned int new_node, unsigned int new_supernode, bool bClosed)
		{
			XNode node;

			node.Reset();

			node.Update(cost, score, new_node, new_supernode);

			AddNode(node, bClosed);
		}

		void XAStar::Clear(IResult *result, int cost)
		{
			result->count = 0;
			result->cost = cost;
			result->nodes.clear();
		}

		int XAStar::GetLowestNodeIndex(bool bClosed)
		{
			int lowestIdx = -1;
			int lowestVal = INT_MAX;
			for (int i = 0, val, c = GetCount(bClosed); i < c; ++i)
			{
				if ((val = NodeAtIndex(i, bClosed)->f) < lowestVal)
				{
					lowestVal = val;
					lowestIdx = i;
				}
			}

			return lowestIdx;
		}

		void XAStar::Search(IData *data, unsigned int begin, unsigned int goal, IResult *result)
		{
			
			// initialize & clear data
			
			XNode *current = new XNode;

			current->Reset();

			Clear(result, -1);

			int idx[3];

			ForBool(Closed, {

				idx[bClosed] = -1;
				nodes[bClosed].clear();
				SetCount(0, bClosed);

			});

			// tell to data provider that we about to begin 

			data->PerpareGathering();

			// add first node aka begin/root node from provieded node ID's

			AddNode(0, data->CalculateH(begin, goal), begin, begin, false);

			do
			{
				// find the node with lowest rank
				
				idx[2] = GetLowestNodeIndex(false);
				
				if (idx[2] < 0)
				{
					break;
				}

				// remove the node from OPEN list and set as current node

				RemoveNodeAtIndex(current, idx[2], false);

				// tell to data provider that current node changed

				data->UpdateCurrentNode(current->node, current->supernode);

				// if it matches the goal, we are done!

				if (data->Match(goal))
				{
					Reconstruct(data, result, current);

					break;
				}

				// add it to closed

				AddNode(*current, true);

				// requests from data provider node ID's at current state to goal

				data->GatherData(goal);

				// proccess gathered ID's for updating OPEN & CLOSE lists

				for (int i = 0, c = 0; i < data->count; ++i, c = 0)
				{
					const int cost = current->g + data->costs[i];
					
					// removed existing node if greater than specified cost   
					ForBool(Closed, {

						idx[bClosed] = IdxInNode(data, data->nodes[i], bClosed);

						if (ValidIdx(idx[bClosed], bClosed))
						{
							RemoveNodeAtIndex(NULL, idx[bClosed], bClosed, cost);
						}
						else
						{
							c++;
						}
					});

					// if node not existing then add to OPEN list
					if (c == 2)
					{
						AddNode(cost, data->CalculateH(i, goal), data->nodes[i], current->node, false);
					}
				}

			} while (true);

			delete current;
		}
	}
}