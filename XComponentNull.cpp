#include "XComponentNull.h"

namespace Utils
{
	namespace Component
	{
		void XNull::Initialize(XBase *entity)
		{
			eDependence = EDependence::None;
		}

		void XNull::Update(XBase *entity) { }

		void XNull::Shutdown(XBase *entity) { }
	}
}