#pragma once

#include "XBuffer.h"

namespace Utils
{
	template <typename TBlock> class TValueBuffer : public XBuffer<TBlock>
	{
	public:
		
		TValueBuffer() : XBuffer<TBlock>() { }
		TValueBuffer(const size_t kMaxMemory) : XBuffer<TBlock>(kMaxMemory) { }
		TValueBuffer(const TValueBuffer& aValueBuffer) : XBuffer<TBlock>(aValueBuffer) { }
		TValueBuffer& Init(const size_t& kMaxMemory)
		{
			XBuffer<TBlock>::Init(kMaxMemory);
			
			return *this;
		}

		TValueBuffer& Release()
		{
			XBuffer<TBlock>::Release();
		
			return *this;
		}

		size_t GetOffset()
		{
			return XBuffer<TBlock>::GetOffset();
		}

		void* Get(size_t iIndex)
		{
			return XBuffer<TBlock>::Get(iIndex);
		}

		XT(Value) size_t GetOffset()
		{
			return m_iOffset - sizeof(TValue);
		}

		XT(Value) TValue* Set(const TValue hValue, size_t *pOffset = NULL) // Creating new value with data, outputs offset
		{
			Set<TValue>(pOffset);

			return Set<TValue>(hValue, GetOffset<TValue>());
		}

		XT(Value) TValue* Set(size_t *pOffset = NULL) // Creating new value, outputs offset
		{
			if (m_iOffset >= m_iSize)
			{
				return NULL;
			}

			TValue* pValue = new ((void*)((uintptr_t)m_pBuffer + m_iOffset)) TValue;
	        
			if (pOffset != NULL)
			{
				(*pOffset) = m_iOffset;
			}

			m_iOffset += sizeof(TValue);
			
			return pValue;
		}

		XT(Value) TValue* Set(const TValue hValue, size_t iOffset) // Setting existing value at offset
		{
			TValue *pValue = Get<TValue>(iOffset);

			if (pValue)
			{
				(*pValue) = hValue;
			}

			return pValue;
		}

		XT(Value) TValue* Get(size_t iOffset) // Getting value at offset
		{
			return (TValue*)Get(iOffset);
		}
	};

	class XValueBuffer : public TValueBuffer<bool>
	{
	public:
		XValueBuffer(const size_t kMaxMemory) : TValueBuffer<bool>(kMaxMemory) {} 
	};
}