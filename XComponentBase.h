#ifndef _UTILS_CPP_COMPONENT_XBASE_H_
#define _UTILS_CPP_COMPONENT_XBASE_H_


#include "IComponentBase.h"
#include "XComponentSet.h" 
#include "XEvent.h"
#include "XDelegate.h"
#include "Base.h"

namespace Utils
{
	namespace Component
	{
		class XBase : public IBase, public XSet<IBase*>
		{
		public:

			struct IEventArgs { };

			typedef Delegate::X2<void, XBase*, IEventArgs*> EventDelegate;

			enum ERequirePolicy
			{
				OnlyGet,
				DoCreate,
				CreateIfNeeded
			};

			void Initialize(XBase *entity);

			void Register(const char *name, EventDelegate d);

			void UnRegister(const char *name = NULL);

			void Send(const char *name, IEventArgs *args = NULL);

			template < void(*TFunction) (XBase *, IEventArgs*) > void RegisterFunction(const char *name)
			{
				Register(name, EventDelegate::FromFunction<TFunction>());
			}

			XTN(Object), void(TObject::*TMethod) (XBase *, IEventArgs*) > void RegisterMethod(const char *name, TObject* m_ptrObject)
			{
				Register(name, EventDelegate::FromMethod<TObject, TMethod>(m_ptrObject));
			}

			XTN(Object), void(TObject::*TMethod) (XBase *, IEventArgs*) const > void RegisterConstMethod(const char *name, const TObject * m_ptrObject)
			{
				Register(name, EventDelegate::FromConstMethod<TObject, TMethod>(m_ptrObject));
			}

			void Update(XBase *entity);

			void Shutdown(XBase *entity);

			XT(Component) TComponent * Require(unsigned int iID = 0)
			{
				return Has < TComponent > (iID) ? Get < TComponent > (iID) : (TComponent*)(0);
			}

			XTN(Interface), typename TComponent> TComponent * Require(unsigned int iID = 0)
			{
				return (TComponent*)(Require < TInterface > (iID));
			}

			XT(Component) TComponent * Require(ERequirePolicy policy, unsigned int iID = 0)
			{
				if (policy == DoCreate)
				{
					return Create < TComponent >(iID);
				}

				if (Has<TComponent>(iID) == false)
				{
					return policy == CreateIfNeeded ? Create < TComponent > (iID) : (TComponent*)(0);
				}

				return (TComponent*)Get < TComponent >(iID);
			}

			XTN(Interface), typename TComponent> TComponent  * Require(ERequirePolicy policy, unsigned int iID = 0)
			{
				if (policy == DoCreate)
				{
					return Create < TInterface, TComponent > (iID);
				}

				if (Has < TInterface > (iID) == false)
				{
					return policy == CreateIfNeeded ? Create < TInterface, TComponent > (iID) : (TComponent*)(0);
				}

				return (TComponent*)Get < TInterface > (iID);
			}

			
			XTN(Interface), typename TComponent> TComponent * Create(unsigned int iID = 0)
			{
				return (TComponent*)XSet < IBase * >::Add(GetKey < TInterface >(), (IBase*)(new TComponent()), iID, true);
			}

			XT(Component) TComponent * Create(unsigned int iID = 0)
			{
				return XSet < IBase * >::Create<TComponent>(iID);
			}

			XT(Component) void Include(TComponent *component, unsigned int iID = 0)
			{
				if (Has<TComponent>(iID) == false)
				{
					Add<TComponent>(component, iID);
				}
			}

		protected:

			Event::XHandler<EventDelegate> eventHandler;
			void InitializeBases(TBases *components);
		};
	}
}
 
#endif