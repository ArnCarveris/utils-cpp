
#pragma once
#include "XStateBase.h"

namespace Utils
{
	namespace State
	{
	
		template <typename TMachine> class CMachine
		{
		public:

			typedef XBase<TMachine> TState;

			CMachine()
			{
				m_pCurrent = TState::XNull::Self();
			}

			virtual ~CMachine()
			{
				m_pCurrent = NULL;
			}

			void Update(float fDeltaTime)
			{
				m_pCurrent->OnUpdate((TMachine*)this, fDeltaTime);
			}

			bool ChangeState(TState *pState)
			{
				 return DoExit() && DoEnter(pState);
			}
			
			TState *GetState()
			{
				return m_pCurrent;
			}


		protected:
			bool DoExit()
			{
				 return CanExit() && m_pCurrent->OnExit();
			}

			bool DoEnter(TState *pState)
			{
				return CanEnter(pState->Validated()) && pState->OnEnter() && (m_pCurrent = pState) != NULL;
			}

			virtual bool CanExit() {return true;}
			virtual bool CanEnter(TState *pState) {return true;};

			TState * m_pCurrent;
		};
	}
}