#ifndef _UTILS_CPP_XDICTIONARY_H_
#define _UTILS_CPP_XDICTIONARY_H_

#include <map>
#include <vector>
#include <algorithm>

namespace Utils
{
	template <typename TKey, typename TValue> class XDictionary
	{
	private:

		std::map<TKey, TValue> data;
		std::vector<TKey> keys;
	public:

		bool Add(TKey key, TValue value)
		{
			bool bWillAdd = Has(key) == false;

			if (bWillAdd)
			{
				keys.push_back(key);
				data.insert(std::pair<TKey, TValue>(key, value));
			}

			return bWillAdd;
		}

		void Copy(XDictionary &dictionary, bool bClear = true)
		{
			TKey key;

			if (bClear)
			{
				Clear();
			}

			for (int i = 0, c = dictionary.GetCount(); i < c; i++)
			{
				key = dictionary.GetKeyAtIndex(i);

				Add(key, *dictionary.Get(key));
			}
		}

		bool Has(TKey key)
		{
			return data.find(key) != data.end();
		}

		bool HasAtIndex(int index)
		{
			return index < keys.size();
		}

		TValue *Get(TKey key)
		{
			return Has(key) ? &data[key] : NULL;
		}

		TValue *GetAtIndex(int index)
		{
			return index < 0 || index >= (int)keys.size() ? NULL : &data[keys[index]];
		}

		TKey GetKeyAtIndex(int index)
		{
			return keys[index];
		}

		void SetWithIndex(int index, TValue value)
		{
			if (index >= 0 && index < keys.size())
			{
				data[keys[index]] = value;
			}
		}

		void Set(TKey key, TValue value)
		{
			if (Add(key, value) == false)
			{
				data[key] = value;
			}
		}

		TValue operator[] (TKey key)
		{
			return data[key];
		}

		size_t GetCount()
		{
			return keys.size();
		}

		void Clear()
		{
			keys.clear();
			data.clear();
		}

		bool Remove(TKey key)
		{
			std::map<TKey, TValue>::iterator it = data.find(key);

			bool bWillRemove = it != data.end();

			if (bWillRemove)
			{
				data.erase(it);

				keys.erase(std::remove(keys.begin(), keys.end(), key), keys.end());
			}

			return bWillRemove;
		}
	};
}

#endif