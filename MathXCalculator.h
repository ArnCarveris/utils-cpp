#pragma once

#include "XIndexedValueBuffer.h"

namespace Utils
{
	namespace Math
	{
		XT(First _XT(Value) _XT(Second)) TValue Calculate(TFirst a, const char* szOp, TSecond b)
		{

#			define op_code(_code_) _code_: return
#			define op(_op_) op_code(#@_op_)

#			define op_ab(_op_) op(_op_) a _op_ b
#			define op_a(_op_)  op(_op_) _op_ a
#			define op_b(_op_)  op(_op_) _op_ b

#			define op_code_ab(_op_, _code_) op_code(_code_) a _op_ b
#			define op_code_a(_op_, _code_)  op_code(_code_) _op_ a
#			define op_code_b(_op_, _code_)  op_code(_code_) _op_ b

#			define op_buffer(_size_) : XValueBuffer hBuffer(_size_); hBuffer.Set<const char*>(szOp)
#			define op_buffer_type(_type_) (*hBuffer.Get<_type_>(0))

#			define op_set(_op_) Set<const char*>(#_op_)

			switch (szOp[0])
			{
				case op_ab(+);
				case op_ab(-);
				case op_ab(*);
				case op_ab(/);
				case op_ab(%);
				case op_a(~);

				default op_buffer(4);
				{
					
					static unsigned short op_code_scope[3];
					static unsigned char op_code_scope_idx = 2;
					
					if (op_code_scope_idx == 2)
					{
						XIndexedValueBuffer hScopeBuffer(10);
						
						{
							hScopeBuffer.op_set(>=);
							hScopeBuffer.op_set(!);
						} {
							op_code_scope[0] = *hScopeBuffer.Get<unsigned short>(0);
							op_code_scope[1] = *hScopeBuffer.Get<unsigned short>(1);
						}
						
						op_code_scope_idx = op_code_scope[0] > op_code_scope[1];
						
						{
							op_code_scope[2] = op_code_scope[1 - op_code_scope_idx] - op_code_scope[op_code_scope_idx];
						}
					}

					unsigned short op_code_idx = op_buffer_type(unsigned short) - op_code_scope[op_code_scope_idx];

					if (op_code_scope_idx)
					{
						op_code_idx = op_code_scope[2] - op_code_idx;
					}

					switch (op_code_idx)
					{
						case op_code_ab(>=, 0x00);
						case op_code_ab(<=, 0x04);
						case op_code_ab(==, 0x08);
						case op_code_ab(!=, 0x0C);
						case op_code_ab(&&, 0x10);
						case op_code_ab(||, 0x14);
						case op_code_ab(>>, 0x18);
						case op_code_ab(<<, 0x1C);
						case op_code_ab(<,	0x20);
						case op_code_ab(>,	0x24);
						case op_code_ab(&,	0x28);
						case op_code_ab(|,	0x2C);
						case op_code_a(!,	0x30);
					}	
					break;
				}
				
			}

#			undef op
#			undef op_ab
#			undef op_a 
#			undef op_b 

#			undef op_set

#			undef op_code
#			undef op_code_ab
#			undef op_code_a
#			undef op_code_b

#			undef op_buffer
#			undef op_buffer_type

			return (TValue)0;
		}
	}
}

/*
typedef enum {ident, number, lparen, rparen, times, slash, plus,
    minus, eql, neq, lss, leq, gtr, geq, callsym, beginsym, semicolon,
    endsym, ifsym, whilesym, becomes, thensym, dosym, constsym, comma,
    varsym, procsym, period, oddsym} Symbol;

Symbol sym;
void getsym(void);
void error(const char msg[]);
void expression(void);

int accept(Symbol s) {
    if (sym == s) {
        getsym();
        return 1;
    }
    return 0;
}

int expect(Symbol s) {
    if (accept(s))
        return 1;
    error("expect: unexpected symbol");
    return 0;
}

void factor(void) {
    if (accept(ident)) {
        ;
    } else if (accept(number)) {
        ;
    } else if (accept(lparen)) {
        expression();
        expect(rparen);
    } else {
        error("factor: syntax error");
        getsym();
    }
}

void term(void) {
    factor();
    while (sym == times || sym == slash) {
        getsym();
        factor();
    }
}

void expression(void) {
    if (sym == plus || sym == minus)
        getsym();
    term();
    while (sym == plus || sym == minus) {
        getsym();
        term();
    }
}

void condition(void) {
    if (accept(oddsym)) {
        expression();
    } else {
        expression();
        if (sym == eql || sym == neq || sym == lss ||
            sym == leq || sym == gtr || sym == geq) {
            getsym();
            expression();
        } else {
            error("condition:valid operator");
            error("condition: invalid operator");
            getsym();
        }
    }
}

void statement(void) {
    if (accept(ident)) {
        expect(becomes);
        expression();
    } else if (accept(callsym)) {
        expect(ident);
    } else if (accept(beginsym)) {
        do {
            statement();
        } while (accept(semicolon));
        expect(endsym);
    } else if (accept(ifsym)) {
        condition();
        expect(thensym);
        statement();
    } else if (accept(whilesym)) {
        condition();
        expect(dosym);
        statement();
    }
}

void block(void) {
    if (accept(constsym)) {
        do {
            expect(ident);
            expect(eql);
            expect(number);
        } while (accept(comma));
        expect(semicolon);
    }
    if (accept(varsym)) {
        do {
            expect(ident);
        } while (accept(comma));
        expect(semicolon);
    }
    while (accept(procsym)) {
        expect(ident);
        expect(semicolon);
        block();
        expect(semicolon);
    }
    statement();
}

void program(void) {
    getsym();
    block();
    expect(period);
}

*/
