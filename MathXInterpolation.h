#ifndef UTILS_CPP_MATH_INTERPOLATION_H
#define UTILS_CPP_MATH_INTERPOLATION_H

#include <vector>

#include "MathXNumber.h"
#include "XDelegate.h"

namespace Utils
{
	namespace Math
	{	
		template <typename T> static inline void Remap(const T& kaValue, const T& kaBeginA, const T& kaEndA, const T& kaBeginB, const T& kaEndB, T& aOut)
		{
			aOut = (kaValue - kaBeginA) / (kaEndA - kaBeginA) * (kaEndB - kaBeginB) + kaBeginB;
		}

		template <typename T> static inline void Map(const T& kaValue, const T& kaBeginA, const T& kaEndA, const T& kaBeginB, const T& kaEndB, T& aOut)
		{
			aOut = kaBeginB + (kaEndB - kaBeginB) * ((kaValue - kaBeginA) / (kaEndA - kaBeginA));
		}

		template <typename T, typename F> static inline void LerpComponent(const F& kfProgress, const T& kaBegin, const T& kaEnd, T& aOut)
		{
			aOut = kaBegin + (T)(kfProgress * (F)(kaEnd - kaBegin));
		}
		
		template <typename T> static inline void Lerp(const float kfProgress, const T& kaBegin, const T& kaEnd, T& aOut)
		{
			aOut = kaBegin + kfProgress * (kaEnd - kaBegin);
		}

		template <typename T> struct SGradient
		{
			struct SData;
			struct SDynamic;
			struct SStorage;
			
			typedef Utils::Delegate::X4<void, const float&, SData*, SData*, T&> TUpdateDelegate;
			typedef Utils::Delegate::X3<void, const float&, SData*, SData*> TCustomUpdateDelegate;

			template<size_t nCount> struct SFixed
			{
				SData m_aData[nCount];

				inline size_t GetCount() const
				{
					return nCount;
				}

				inline SData& operator[] (const size_t& i)
				{
					return m_aData[i];
				}
			};

			inline void Set(const SDynamic& aDynamic)
			{
				m_pBegin = &aDynamic[0];
				m_pEnd = m_pBegin + aDynamic.GetCount();
			}

			template<size_t N> inline void Set(const SFixed<N>& sFixed)
			{
				m_pBegin = &sFixed[0];
				m_pEnd = &sFixed[N];
			}

			
			template<size_t N> inline void Set(SData(&aData)[N])
			{
				m_pBegin = &aData[0];
				m_pEnd = &aData[N];
			}
			inline void Set(SStorage& rsStorage)
			{
				m_pBegin = &rsStorage[0];
				m_pEnd = &rsStorage[rsStorage.GetSize()];
			}

			inline void Reset()
			{
				m_pCurrent = m_pBegin;
				m_pNext = m_pCurrent + 1;
			}

			inline bool UpdateProgress(const float& kfProgress)
			{
				bool bChanged = false;

				if (m_pCurrent->m_fProgress > kfProgress)
				{
					if ((bChanged = m_pCurrent != m_pBegin))
					{
						m_pCurrent--;
						m_pNext--;
					}
					
				}
				else if (m_pNext->m_fProgress < kfProgress)
				{
					if ((bChanged = m_pNext != m_pEnd - 1))
					{
						m_pCurrent++;
						m_pNext++;						
					}
				}

				return bChanged;
			}

			inline void ConvertProgress(const float& kfProgress, float &fOut)
			{
				const bool kbBegin = kfProgress <= m_pCurrent->m_fProgress;
				const bool kbEnd = kfProgress >= m_pNext->m_fProgress;

				if (!kbBegin && !kbEnd)
				{
					Remap(kfProgress, m_pCurrent->m_fProgress, m_pNext->m_fProgress, 0.0f, 1.0f, fOut);
				}
				else
				{
					fOut = kbEnd ? 1.0f : 0.0f;
				}
			}

			template<bool bComponent> inline void UpdateFull(const float& krfProgress, T& rOut)
			{
				while(UpdateProgress(krfProgress));
				{
					float fProgress;

					ConvertProgress(krfProgress, fProgress);

					SData::Get<bComponent>(fProgress, m_pCurrent, m_pNext, rOut); 
				}
			}

			template<bool bComponent> inline void Update(const float& kfProgress, T& aOut)
			{
				float fProgress;

				ConvertProgress(kfProgress, fProgress);
				SData::Get<bComponent>(fProgress, m_pCurrent, m_pNext, aOut);
				UpdateProgress(kfProgress);
			}

			inline void Update(const float& kfProgress, T& aOut, const TUpdateDelegate& kaDelegate)
			{
				float fProgress;

				ConvertProgress(kfProgress, fProgress);
				kaDelegate(fProgress, m_pCurrent, m_pNext, aOut);
				UpdateProgress(kfProgress);
			}

			inline void Update(const float& kfProgress, const TCustomUpdateDelegate& kaDelegate)
			{
				float fProgress;

				ConvertProgress(kfProgress, fProgress);
				kaDelegate(fProgress, m_pCurrent, m_pNext);
				UpdateProgress(kfProgress);
			}

			SData* m_pBegin;
			SData* m_pEnd;
			SData* m_pCurrent;
			SData* m_pNext;
		};

		
		template <typename T> struct SGradient<T>::SData
		{
			T m_aT;
			float m_fProgress;

			inline SData& Set(const T& kaT, const float& kfProgress)
			{
				m_aT = kaT;
				m_fProgress = kfProgress;

				return *this;
			}
			
			template<bool bComponent> static inline void Get(const float& kfProgress, SData* pCurrent, SData* pNext, T& aOut)
			{
				LerpComponent<T, float>(kfProgress, pCurrent->m_aT, pNext->m_aT, aOut);
			}

			template<> static inline void Get<false>(const float& kfProgress, SData* pCurrent, SData* pNext, T& aOut)
			{
				Lerp<T>(kfProgress, pCurrent->m_aT, pNext->m_aT, aOut);
			}
		};
		
		template <typename T> struct SGradient<T>::SDynamic
		{		
			SDynamic()
			{
				Allocate(0);
			}
			SDynamic(const size_t& knCount)
			{
				Allocate(knCount);
			}

			~SDynamic()
			{
				Deallocate();
			}

			inline void Allocate(const size_t& nCount)
			{
				m_pData = nCount ? new SData[nCount] : 0;
				m_nData = nCount;
			}

			inline void Deallocate()
			{
				if (m_pData)
				{
					delete[] m_pData;
					m_pData = NULL;
				}
				m_nData = 0;
			}

			inline const size_t& GetCount() const
			{
				return m_nData;
			}

			inline SData& operator[] (const size_t& i)
			{
				return m_pData[i];
			}

			SData* m_pData;
			size_t m_nData;			
		};
		
		template <typename T> struct SGradient<T>::SStorage
		{
			typedef SGradient<T> TGradient;
			typedef std::vector<SData> TVector;

			TGradient m_aGradient;
			TVector m_aVector;


			struct SKey
			{
				float m_fTarget;
				float m_fDistance;
				int m_nIndex;
				typename TVector::iterator m_cIterator;

				SKey(const float kfTarget) : 
					m_fTarget(kfTarget),
					m_fDistance(FLT_MAX),
					m_cIterator()
				{ }

				inline bool operator() (typename TVector::iterator it)
				{
					float fDistance = fabsf(it->m_fProgress - m_fTarget);
					
					if (m_fDistance > fDistance)
					{
						m_fDistance = fDistance;
						m_cIterator = it;

						return true;
					}

					return false;
				}
			};

			struct SSide
			{
				float m_fProgress;
				int m_nSign;

				typename TVector::iterator m_cLowest;
				
				typename TVector::iterator m_cBegin;
				typename TVector::iterator m_cEnd;

				inline void Init(TVector* pcVector, SKey* psTemp, const float kfValue, const float kfMin, const float kfMax)
				{
					const size_t knSize = pcVector->size();

					m_cBegin = psTemp[0].m_cIterator; 
					m_cEnd = psTemp[1].m_cIterator;

					if (m_cBegin > m_cEnd)
					{
						m_cBegin -= knSize - 1;
					}

					Remap(kfValue, kfMin, kfMax, m_cBegin->m_fProgress, m_cEnd->m_fProgress, m_fProgress);
					
					if (kfMin < kfMax)
					{
						m_nSign = 1;
						
						if (++m_cBegin == pcVector->end())
						{
							m_cBegin = pcVector->begin();
						}
					}
					else
					{
						m_nSign = -1;

						if (m_cEnd == pcVector->begin())
						{
							m_cEnd = pcVector->end() - 1;
						}
						else
						{
							m_cEnd--;
						}
					}

					if (kfValue > 0.0f)
					{
						m_cLowest = std::find_if(m_cBegin, m_cEnd, *this);
					}
					else
					{
						m_cLowest = kfMin < kfMax ? m_cBegin : m_cEnd;
					}
					
				}

				inline bool operator() (const SData& krData)
				{
					return krData.m_fProgress >= m_fProgress;
				}

				inline typename TVector::iterator GetSignedLowest()
				{
					return m_cLowest + m_nSign;
				}

				inline void Update(TVector* pcVector, const TUpdateDelegate& krDelegate)
				{
					float fProgress;
					union
					{
						SData* aps[2]; 
						struct 
						{						
							SData* current;
							SData* next;
						} ps;
					} uData;

					uData.aps[m_nSign > 0] = &*m_cLowest;
					uData.aps[m_nSign < 0] = &*(m_cLowest - m_nSign);

					Remap(m_fProgress, uData.ps.current->m_fProgress, uData.ps.next->m_fProgress, 0.0f, 1.0f, fProgress);

					krDelegate(fProgress, uData.ps.current, uData.ps.next, uData.aps[m_nSign > 0]->m_aT);
				}
			};

			struct SDescription
			{
				struct SNumeric
				{
					SNumber::SetAtAddressThunk* m_paThunk;
					size_t m_nThunk;

					inline void Init(const SNumeric& krOther)
					{
						m_paThunk = krOther.m_paThunk;
						m_nThunk = krOther.m_nThunk;
					}

					template<size_t N> inline void Init(SNumber::SetAtAddressThunk(&raThunk)[N])
					{
						m_paThunk = &raThunk[0];
						m_nThunk = N;
					}
					
					inline void SetAtAddress(SNumber& rNumber, void* pBaseAddress)
					{
						size_t nIndex = rNumber.m_aIndex.m_nValue;

						if (nIndex >= m_nThunk)
						{
							nIndex = m_nThunk - 1;
						}

						(rNumber.*m_paThunk[nIndex])((size_t*)pBaseAddress + rNumber.m_aIndex.m_nValue);
					}

					inline void Load(SNumber& rsNumber, TVector& rVector)
					{
						SData aData;

						void *pBaseAddress = &aData.m_aT;

						for(;rsNumber;rsNumber++)
						{
							switch (rsNumber.GetType())
							{
							case SNumber::Value_Next://next value
								{
									SetAtAddress(rsNumber, pBaseAddress);
								} break;

							case SNumber::Value_LastInSet://set progress
								
								rsNumber.GetFloat<0>(aData.m_fProgress);

								break;

							case SNumber::Value_Last://push data
								{
									SetAtAddress(rsNumber, pBaseAddress);

									rVector.push_back(aData);
								} break;
							}
						}
					}
				} m_aNumeric;

			} m_aDescription;

			
			SStorage()
			{
				Reset();
			}

			SStorage(const SStorage& krsStorage) : m_aVector(krsStorage.m_aVector)
			{
				m_aGradient.Set(*this);
			}

			SStorage(const size_t knSize)
			{
				Resize(knSize);
			}

			inline void Add(const float kfProgress, const T& krT)
			{
				m_aVector.push_back(SData().Set(krT, kfProgress));
			}

			inline void Resize(const size_t knSize)
			{
				m_aVector.resize(knSize);
			}

			inline void AlterRange(const float kfA, const float kfB, const float kfPeak, const float kfScale)
			{
				AlterRange(kfA, kfB, kfPeak, kfScale, TUpdateDelegate::FromFunction<&SData::Get<false> >());
			}

			inline void AlterRange(const float kfA, const float kfB, const float kfPeak, const float kfScale, const TUpdateDelegate& krUpdateDelegate)
			{
				if (kfScale >= 1.0f)
				{
					return;
				}

				SKey asTemp[] = 
				{
					kfA, kfPeak, kfB
				};

				size_t nTemp = sizeof(asTemp) / sizeof(asTemp[0]);

				for(TVector::iterator it = m_aVector.begin(); it != m_aVector.end(); it++)
				{
					for(size_t i = 0; i < nTemp; i++)
					{
						if (asTemp[i](it))
						{
							
						}
					}
				}
				
				
				SSide sA, sB;
				
				sA.Init(
					&m_aVector, &asTemp[0],
					kfScale, 0.0f, 1.0f
				);

				sB.Init(
					&m_aVector, &asTemp[1], 
					kfScale, 1.0f, 0.0f
				);

				const bool kbSame = sB.m_cLowest == sA.m_cLowest;
				
				float fCenter;
				SData* psCenterBase;

				if(kbSame)
				{
					fCenter = kfScale;

					psCenterBase = &*asTemp[0].m_cIterator;
				}
				else
				{
					fCenter = 0.001f;

					psCenterBase = &*sA.m_cLowest;
				}

				krUpdateDelegate(fCenter, psCenterBase, &*asTemp[1].m_cIterator,asTemp[1].m_cIterator->m_aT);
								
				if (kbSame)
				{
					return;
				}
				
				sA.Update(&m_aVector, krUpdateDelegate);
				sB.Update(&m_aVector, krUpdateDelegate);

				const size_t kncit = 4;
				
				TVector::iterator acit[kncit];
				
				size_t anidx[kncit];

				if (sA.m_cLowest < sB.m_cLowest)
				{
					acit[0] = asTemp[1].m_cIterator + 1;
					acit[1] = sB.GetSignedLowest();
					acit[2] = sA.GetSignedLowest();
					acit[3] = asTemp[1].m_cIterator - 1;				
				}
				else
				{
					acit[0] = sA.GetSignedLowest();
					acit[1] = asTemp[1].m_cIterator - 1;
					acit[2] = asTemp[1].m_cIterator + 1;
					acit[3] = sB.GetSignedLowest();
				}

				for (size_t i = 0; i < kncit; i++)
				{
					if (acit[i] < m_aVector.begin())
					{
						anidx[i] = m_aVector.size() - 1 - (m_aVector.begin() - acit[i]);
					}
					else if (acit[i] >= m_aVector.end())
					{
						anidx[i] = acit[i] - m_aVector.end();
					}
					else
					{
						anidx[i] = acit[i] - m_aVector.begin();
					}
				}
				for (size_t i = 0; i < kncit; i+=2)
				{
					if (anidx[i] == anidx[i+1])
					{
						m_aVector.erase(m_aVector.begin() + anidx[i]);
					}
					else if (anidx[i] < anidx[i+1])
					{
						m_aVector.erase(m_aVector.begin() + anidx[i], m_aVector.begin() + anidx[i+1] + 1);
					}
					else
					{
						
					}
				}
			}
			

			inline void Reassign()
			{
				m_aGradient.Set(*this);
			}

			inline void Reset()
			{
				m_aGradient.Reset();
				m_aVector.clear();
			}
		
			inline void LoadText(const char *szText)
			{				
				m_aVector.clear();

				if (szText && szText[0])
				{
					m_aDescription.m_aNumeric.Load(SNumber(szText), m_aVector);
				}

				Reassign();
			}


			inline SData& operator[] (const size_t& i)
			{
				return m_aVector[i];
			}

			inline const size_t GetSize() const
			{
				return m_aVector.size();
			}
		};
	}
}

#endif