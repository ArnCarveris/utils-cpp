#ifndef _UTILS_CPP_COMPONENT_XNULL_H_
#define _UTILS_CPP_COMPONENT_XNULL_H_

#include "IComponentBase.h"

namespace Utils
{
	namespace Component
	{
		class XNull : public IBase
		{
		public:

			void Initialize(XBase *entity);

			void Update(XBase *entity);

			void Shutdown(XBase *entity);

		};
	}
}

#endif