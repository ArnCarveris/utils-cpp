#undef TCallback

XTN(Return) SEPARATOR TEMPLATE_PARAMS > class SRUTIL_DELEGATE_CLASS_NAME
#ifdef SRUTIL_DELEGATE_PREFERRED_SYNTAX
<TReturn(TEMPLATE_ARGS)>
#endif
{
public:

	typedef Invoker::XN(X) < TReturn SEPARATOR TEMPLATE_PARAMS > TInvoker;

#define TCallback(_name_) static SRUTIL_DELEGATE_CLASS_NAME From##_name_

	SRUTIL_DELEGATE_CLASS_NAME() : m_ptrObject(0), m_ptrFunction(0) { }

	XT(Return(*TMethod) (TEMPLATE_ARGS)) TCallback(Function) ()
	{
		return From(0, &Function < TMethod >);
	}

	XTN(Object), TReturn(TObject::*TMethod) (TEMPLATE_ARGS) > TCallback(Method) (TObject* m_ptrObject)
	{
		return From(m_ptrObject, &Method < TObject, TMethod > );
	}

	XTN(Object), TReturn(TObject::*TMethod) (TEMPLATE_ARGS) const > TCallback(ConstMethod) (TObject const* m_ptrObject)
	{
		return From(const_cast < TObject* > (m_ptrObject), &ConstMethod < TObject, TMethod >);
	}

	TReturn operator()(PARAMS) const
	{
		return (*m_ptrFunction)(m_ptrObject SEPARATOR ARGS);
	}

	operator bool() const
	{
		return m_ptrFunction != 0;
	}

	bool operator!() const
	{
		return !(operator bool());
	}

#undef TCallback

private:

#define TCallback(_name_, _arg_) static TReturn SRUTIL_DELEGATE_CALLTYPE _name_ (void* _arg_ SEPARATOR PARAMS)

	typedef TReturn(SRUTIL_DELEGATE_CALLTYPE *TFunction)(void* m_ptrObject SEPARATOR PARAMS);

	static SRUTIL_DELEGATE_CLASS_NAME From(void* ptrObject, TFunction ptrFunction)
	{
		SRUTIL_DELEGATE_CLASS_NAME d;

		d.m_ptrObject = ptrObject;
		d.m_ptrFunction = ptrFunction;

		return d;
	}

	XT(Return(*TMethod) (TEMPLATE_ARGS)) TCallback(Function, )
	{
		return (TMethod)(ARGS);
	}

	XTN(Object), TReturn(TObject::*TMethod) (TEMPLATE_ARGS) > TCallback(Method, m_ptrObject)
	{
		TObject* p = static_cast < TObject* > (m_ptrObject);

		return (p->*TMethod)(ARGS);
	}

	XTN(Object), TReturn(TObject::*TMethod) (TEMPLATE_ARGS) const > TCallback(ConstMethod, m_ptrObject)
	{
		TObject const* p = static_cast < TObject* > (m_ptrObject);

		return (p->*TMethod)(ARGS);
	}


	void*			m_ptrObject;
	TFunction		m_ptrFunction;
};

#undef TCallback