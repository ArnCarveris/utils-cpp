#include "../Base.h"

XTN(Return) SRUTIL_DELEGATE_SEPARATOR SRUTIL_DELEGATE_TEMPLATE_PARAMS > class XN(I)
#ifdef SRUTIL_DELEGATE_PREFERRED_SYNTAX
< TReturn(SRUTIL_DELEGATE_TEMPLATE_ARGS) >
#endif
: public XBase < TReturn >{
public:
	XN(I) (SRUTIL_DELEGATE_PARAMS) : XBase < TReturn >()

#if SRUTIL_DELEGATE_PARAM_COUNT > 0
		,
#endif
		SRUTIL_DELEGATE_INVOKER_INITIALIZATION_LIST{}

	TReturn operator()(SRUTIL_DELEGATE_PARAMS)
	{
		Prepare(SRUTIL_DELEGATE_ARGS);

		Call();

		return m_xValue;
	}

protected:

	virtual void Prepare(SRUTIL_DELEGATE_PARAMS) = 0;

	virtual void Invoke(SRUTIL_DELEGATE_PARAMS) = 0;

	NOINLINE void InitalizeStack()
	{
		unsigned char stack_buffer[1 << 16];

		m_bInitialized = ptrdiff_t(stack_buffer) != 0;

		Invoke(SRUTIL_DELEGATE_ARGS);
	}

	bool Call()
	{

		if (setjmp(m_iPoint[0]) == 0)
		{
			if (m_bInitialized)
			{
				longjmp(m_iPoint[1], 3);
			}
			else
			{
				InitalizeStack();

				return true;
			}
		}

		return false;
	}

	SRUTIL_DELEGATE_INVOKER_DATA
};