/*
	(c) Sergey Ryazanov (http://home.onego.ru/~ryazanov)

	Template file. May be included many times with different predefined macros.
*/


#if SRUTIL_DELEGATE_PARAM_COUNT > 0
#define SRUTIL_DELEGATE_SEPARATOR ,
#else
#define SRUTIL_DELEGATE_SEPARATOR
#endif

// see BOOST_JOIN foTReturn explanation
#define SRUTIL_DELEGATE_JOIN_MACRO( X, Y) SRUTIL_DELEGATE_DO_JOIN( X, Y )
#define SRUTIL_DELEGATE_DO_JOIN( X, Y ) SRUTIL_DELEGATE_DO_JOIN2(X,Y)
#define SRUTIL_DELEGATE_DO_JOIN2( X, Y ) X##Y


#ifdef SRUTIL_DELEGATE_PREFERRED_SYNTAX
#define XN(_name_) _name_
#else
#define XN(_name_) SRUTIL_DELEGATE_JOIN_MACRO(_name_,SRUTIL_DELEGATE_PARAM_COUNT)
#endif

#define SRUTIL_DELEGATE_CLASS_NAME XN(X)
#define SRUTIL_DELEGATE_INVOKER_CLASS_NAME XN(X) 

#include "def_delegate.h"

namespace Utils
{
	namespace Delegate
	{
		namespace Invoker
		{
			#include "invoker.h"
		}

		#include "delegate.h"
		
		namespace Coroutine
		{
			#include "dcoroutine.h" // (D)elegates for Coroutine (<TReturn, Coroutine*, ...>
			#include "icoroutine.h" // (I)intereface for inherieted Coroutine
			#include "xcoroutine.h" // (X)Template Base for Coroutine based on (D)elegate
		}
	}
}

#include "undef_delegate.h"
#undef XN
#undef TCallback
#undef SRUTIL_DELEGATE_CLASS_NAME
#undef SRUTIL_DELEGATE_INVOKER_CLASS_NAME
#undef SRUTIL_DELEGATE_SEPARATOR
#undef SRUTIL_DELEGATE_JOIN_MACRO
#undef SRUTIL_DELEGATE_DO_JOIN
#undef SRUTIL_DELEGATE_DO_JOIN2
