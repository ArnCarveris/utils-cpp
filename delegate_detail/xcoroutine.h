#include "../Base.h"

XTN(Return) SRUTIL_DELEGATE_SEPARATOR SRUTIL_DELEGATE_TEMPLATE_PARAMS > class XN(X)
#ifdef SRUTIL_DELEGATE_PREFERRED_SYNTAX
< TReturn(SRUTIL_DELEGATE_TEMPLATE_ARGS) >
#endif
: public XBase < TReturn >{

public:

	typedef Delegate::XN(X) <void SRUTIL_DELEGATE_SEPARATOR SRUTIL_DELEGATE_TEMPLATE_ARGS> TCallback;
	typedef XN(D) <void, XBase <TReturn> * SRUTIL_DELEGATE_SEPARATOR SRUTIL_DELEGATE_TEMPLATE_ARGS> TDelegate;


	XN(X) (SRUTIL_DELEGATE_PARAMS) : XBase < TReturn >()
#if SRUTIL_DELEGATE_PARAM_COUNT > 0
		,
#endif
		SRUTIL_DELEGATE_INVOKER_INITIALIZATION_LIST{}

	TReturn operator()(TDelegate d)
	{
		Call(d);

		return m_xValue;
	}

	TReturn operator()(TDelegate d, TCallback c)
	{
		c(SRUTIL_DELEGATE_ARGS);

		return operator()(d);
	}

#if SRUTIL_DELEGATE_PARAM_COUNT > 0


	TReturn operator()(TDelegate d, TCallback c, SRUTIL_DELEGATE_PARAMS)
	{
		c(SRUTIL_DELEGATE_ARGS);

		return operator()(d);
	}
#endif
protected:

	NOINLINE void InitalizeStack(TDelegate d)
	{
		unsigned char stack_buffer[1 << 16];

		m_bInitialized = ptrdiff_t(stack_buffer) != 0;

#if SRUTIL_DELEGATE_PARAM_COUNT > 0

		d(this, SRUTIL_DELEGATE_ARGS);
#else
		d(this);
#endif
	}

	bool Call(TDelegate d)
	{
		if (setjmp(m_iPoint[0]) == 0)
		{
			if (m_bInitialized)
			{
				longjmp(m_iPoint[1], 3);
			}
			else
			{
				InitalizeStack(d);

				return true;
			}
		}

		return false;
	}

	SRUTIL_DELEGATE_INVOKER_DATA
};