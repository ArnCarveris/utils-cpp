#ifndef _UTILS_CPP_XBITS_H_
#define _UTILS_CPP_XBITS_H_

namespace Utils
{
	namespace Bit
	{
		unsigned int GetCount(unsigned int n = 0);

		void Print(unsigned int n);
	}
}

#endif