#ifndef _UTILS_CPP_MANAGER_H_
#define _UTILS_CPP_MANAGER_H_

#include "XDictionary.h"
#include "XSet.h"
#include "XItem.h"
#include "Base.h"

namespace Utils
{

	XTN(Item), typename TItemSet> class XManager
	{
	public:

		typedef XSet<TItemSet> TSet;

		XManager() { }

		~XManager() // TODO: implement deleting
		{
			items.Clear();
			itemSet.Clear();
		}

		void Register(TItem item)
		{
			((XItem*)item)->SetID((unsigned int)items.GetCount() + 1);

			items.Add(((XItem*)item)->GetName(), item);
		}

		void UnRegister(TItem item)
		{
			((XItem*)item)->SetID(0);

			items.Remove(((XItem*)item)->GetName());
		}

		TItem GetByID(unsigned int iID)
		{
			return iID > 0 ? *items.GetAtIndex((int)iID - 1) : NULL;
		}

		TItem GetByName(const char *szName)
		{
			return *items.Get(szName);
		}

		TItem GetFromNamedSetAtIndex(const char *szName, int index)
		{
			return GetFromSetAtIndex(GetNamedSet(szName), index);
		}

		TItem GetFromSetAtIndex(TSet *set, int index)
		{
			return index < 0 || (size_t)index >= set->GetSize() ? NULL : GetByItemSet((*set)[index]);
		}

		XT(NamedSet) TNamedSet* GetNamedSet(const char *szName)
		{
			if (itemSet.Has(szName) == false)
			{
				itemSet.Add(szName, new TNamedSet());
			}

			return (TNamedSet*)(*itemSet.Get(szName));
		}

		bool RemoveNamedSet(const char *szName)
		{
			return itemSet.Remove(szName);
		}

		void Clear()
		{
			items.Clear();
			itemSet.Clear();
		}

	protected:

		XDictionary < const char*, TItem> items;
		XDictionary < const char*, TSet*> itemSet;

		virtual TItem GetByItemSet(TItemSet item) = 0;
	};
}

#endif